<?php

use Illuminate\Database\Seeder;

class DataRowsTableSeederPlus extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('data_rows')->insert(array (
            0 => 
            array (
                'id' => 77,
                'data_type_id' => 2,
                'field' => 'custom_header_code',
                'type' => 'rich_text_box',
                'display_name' => 'CUSTOM HEADER CODE',
                'required' => 1,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'details' => '',
                'order' => 13,
            ),
            1 => 
            array (
                'id' => 78,
                'data_type_id' => 2,
                'field' => 'custom_footer_code',
                'type' => 'rich_text_box',
                'display_name' => 'CUSTOM FOOTER CODE',
                'required' => 1,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'details' => '',
                'order' => 14,
            ),
            2 => 
            array (
                'id' => 79,
                'data_type_id' => 2,
                'field' => 'custom_css',
                'type' => 'text_area',
                'display_name' => 'CUSTOM CSS',
                'required' => 1,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'details' => '{"default":"<style type=\"text\/css\">\n\/* add custom style here *\/\n\n<\/style>"}',
                'order' => 15,
            ),
            3 => 
            array (
                'id' => 80,
                'data_type_id' => 8,
                'field' => 'active_on_homepage',
                'type' => 'checkbox',
                'display_name' => 'Show on Home Page',
                'required' => 1,
                'browse' => 0,
                'read' => 1,
                'edit' => 1,
                'add' => 1,
                'delete' => 1,
                'details' => NULL,
                'order' => 7,
            ),
         ));
    }
}
