<?php

use Illuminate\Database\Seeder;

class DataRowsTableSeederChangeFieldsType extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('data_rows')->where(['data_type_id'=>8,'field' => 'features'])->update(['type' => 'text_area','details'=>'{}']);
        \DB::table('data_rows')->where(['data_type_id'=>9,'field' => 'description'])->update(['type' => 'rich_text_box']);
        
    }
}
