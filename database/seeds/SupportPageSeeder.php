<?php

use Illuminate\Database\Seeder;

class SupportPageSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pages')->where('slug','support')->delete();
        
        \DB::table('pages')->insert(array (
            0 => 
            array (
                'id' => 3,
                'author_id' => 1,
                'title' => 'Get Support From Our Friendly Team',
                'excerpt' => 'Get Support From Us Via Email',
                'body' => '<h2>Enter Your Query Below Or Use The Chat Icon</h2>
<p>Please enter a detailed description of your support inquiry below and we will get back to you via email as soon as we can.</p>',
                'image' => null,
                'slug' => 'support',
                'meta_description' => 'Get Support From Our Friendly Team',
                'meta_keywords' => 'Get Support From Our Friendly Team',
                'status' => 'ACTIVE',
                'created_at' => '2020-06-12 11:45:47',
                'updated_at' => '2020-06-12 11:45:47',
                'custom_css' => '<style type="text/css">
/* add custom style here */

</style>',
            ),
        ));
        
        
    }
}