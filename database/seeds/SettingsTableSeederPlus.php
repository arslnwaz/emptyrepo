<?php

use Illuminate\Database\Seeder;

class SettingsTableSeederPlus extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('settings')->insert(array (
            0 =>
            array (
                'id' => 18,
	            'key' => 'admin.custom_header_code',
	            'display_name' => 'CUSTOM HEADER CODE',
                'value' => '',
                'details' => NULL,
	            'type' => 'rich_text_box',
	            'order' => 12,
	            'group' => 'Admin',
            ),
            1 =>
            array (
                'id' => 19,
	            'key' => 'admin.custom_footer_code',
	            'display_name' => 'CUSTOM FOOTER CODE',
                'value' => '',
                'details' => NULL,
	            'type' => 'rich_text_box',
	            'order' => 13,
	            'group' => 'Admin',
            ),
            2 =>
            array (
            	'id' => 20,
	            'key' => 'admin.custom_css',
	            'display_name' => 'CUSTOM CSS',
                'value' => '',
	            'type' => 'text_area',
	            'order' => 14,
	            'group' => 'Admin',
	            'value' => '<style type="text/css">
	/*your code here*/

	</style>',
                'details' => NULL,
                
            ),
            3 =>
            array (
                'id' => 21,
	            'key' => 'site.custom_header_code',
	            'display_name' => 'CUSTOM HEADER CODE',
                'value' => '',
                'details' => NULL,
	            'type' => 'rich_text_box',
	            'order' => 12,
	            'group' => 'Site',
            ),
            4 =>
            array (
                'id' => 22,
	            'key' => 'site.custom_footer_code',
	            'display_name' => 'CUSTOM FOOTER CODE',
                'value' => '',
                'details' => NULL,
	            'type' => 'rich_text_box',
	            'order' => 13,
	            'group' => 'Site',
            ),
            5 =>
            array (
                'id' => 23,
	            'key' => 'site.custom_css',
	            'display_name' => 'CUSTOM CSS',
                'value' => '',
                'details' => NULL,
	            'type' => 'text_area',
	            'order' => 14,
	            'group' => 'Site',
	             'value' => '<style type="text/css">
	/*your code here*/

	</style>',
            ),
            6 =>
            array (
                'id' => 24,
	            'key' => 'site.custom_section',
	            'display_name' => 'CUSTOM SECTION',
                'value' => '',
                'details' => NULL,
	            'type' => 'rich_text_box',
	            'order' => 10,
	            'group' => 'Site',
            ),
            7 =>
            array (
                'id' => 25,
	            'key' => 'site.placement',
	            'display_name' => 'PLACEMENT',
                'value' => '',
	            'type' => 'select_dropdown',
	            'order' => 11,
	            'group' => 'Site',
	            'details' => '{
	                        "default" : "pricing",
	                            "options" : {
	                                "features": "Features",
	                                "testimonials": "Testimonials",
	                                "pricing": "Pricing"
	                            }
	                        }'
            ),
     	));
    }
}
