<?php

use Illuminate\Database\Seeder;

class CustomScriptSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('voyager_theme_options')->where('key','support_url')->update(['value' => '/p/support']);  
        \DB::table('menu_items')->where([
            'menu_id' =>2,
            'url' =>'/support',
        ])->update(['url' => '/p/support']); 
        \DB::table('menu_items')->where([
            'menu_id' =>2,
            'title' =>'Support',
        ])->update(['url' => '/p/support']);   
        \DB::table('pages')->where('slug','hello-world')->delete(); 

        \DB::table('data_rows')->where([
            'field' => 'custom_css',
            'data_type_id' => 2,
        ])->update(['details' => '{"default":"<style type=\"text\/css\">\n\/* add custom style here *\/\n\n<\/style>"}']);
        
/*        \DB::table('pages')->where('slug','about')->delete();
        \DB::table('pages')->insert(array (
            0 => 
            array (
                'id' => 2,
                'author_id' => 1,
                'title' => 'About',
                'excerpt' => 'This is the about page.',
                'body' => 'Our CRM is the industry standard for running your business with the ultimate omni-channel, latest marketing tools.',
                'image' => NULL,
                'slug' => 'about',
                'meta_description' => 'About CRM',
                'meta_keywords' => 'About CRM',
                'status' => 'ACTIVE',
                'created_at' => '2018-03-30 03:04:51',
                'updated_at' => '2018-03-30 03:04:51',
                'custom_css' => '<style type="text/css">
// add custom style here 

</style>',
            ),
        ));*/
    }
}