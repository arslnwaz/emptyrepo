<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_sites', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('server_id')->unsigned()->nullable();
            $table->string('site_id');
            $table->string('name')->nullable();
            $table->string('aliases')->nullable();
            $table->string('directory')->nullable();
            $table->boolean('wildcards')->default(0);
            $table->string('site_status')->nullable();
            $table->string('domain')->nullable();
            $table->string('project_type')->nullable();
            $table->boolean('isolated')->default(0);
            $table->string('username')->nullable();
            $table->string('repository')->nullable();
            $table->string('repository_provider')->nullable();
            $table->string('repository_branch')->nullable();
            $table->string('repository_status')->nullable();
            $table->boolean('quick_deploy')->default(0);
            $table->string('deployment_status')->nullable();
            $table->longText('deployment_script')->nullable();
            $table->longText('env_file')->nullable();
            $table->string('app')->nullable();
            $table->string('app_status')->nullable();
            $table->string('hipchat_room')->nullable();
            $table->string('slack_channel')->nullable();
            $table->dateTime('site_created_at')->nullable();
            $table->string('deployment_url')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('server_id')->references('id')->on('user_apps')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_sites');
    }
}
