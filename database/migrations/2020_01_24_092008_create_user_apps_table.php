<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_apps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('server_id')->nullable();
            $table->string('credential_id')->nullable();
            $table->string('app_name')->nullable();
            $table->string('provider')->nullable();
            $table->string('size')->nullable();
            $table->string('region')->nullable();
            $table->string('php_version')->nullable();
            $table->string('ip_address')->nullable();
            $table->string('ssh_port')->nullable();
            $table->string('private_ip_address')->nullable();
            $table->string('blackfire_status')->nullable();
            $table->string('papertrail_status')->nullable();
            $table->boolean('revoked')->default(0);
            $table->dateTime('server_created_at')->nullable();
            $table->boolean('is_ready')->default(0);
            $table->string('database_type')->nullable();
            $table->string('database_name')->nullable();
            $table->longText('network')->nullable();
            $table->string('sudo_password')->nullable();
            $table->string('database_password')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_apps');
    }
}
