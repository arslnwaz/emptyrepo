<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActiveOnHomepageToPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plans', function (Blueprint $table) {
            if(!Schema::hasColumn('plans', 'active_on_homepage'))
            {
                $table->boolean('active_on_homepage')->default(1);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('plans', 'active_on_homepage'))
        {
            Schema::table('plans', function (Blueprint $table) {
                $table->dropColumn('active_on_homepage');
            });
        }

    }
}
