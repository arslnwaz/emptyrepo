<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class UserSite extends Model
{
	protected $guarded = ['id', 'created_at', 'updated_at'];

    public function userid()
    {
        return $this->belongsTo('App\User','foreign_key');
    }

    public function serverid()
    {
        return $this->belongsTo('App\UserApp','foreign_key');
    }
}
