<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Schema\Builder; // Import Builder where defaultStringLength method is defined
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use TCG\Voyager\Facades\Voyager;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->environment() == 'production') {
            $this->app['request']->server->set('HTTPS', true);
        }
        Blade::withoutComponentTags();
        Builder::defaultStringLength(191);
        Voyager::addAction(\App\Actions\ActionDeploy::class);
        Voyager::addAction(\App\Actions\ActionDeployScript::class);
        Voyager::addAction(\App\Actions\ActionDeployNow::class);
        Voyager::addAction(\App\Actions\ActionEnv::class);
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
