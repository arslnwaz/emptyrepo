<?php

namespace App\Http\Controllers\Voyager;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataRestored;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use Validator;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\TooManyRedirectsException;
use GuzzleHttp\Psr7;
use App\UserSite;

class UserSiteController  extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    use BreadRelationshipParser;



    public function index(Request $request)
    {


        // GET THE SLUG, ex. 'posts', 'pages', etc.
        //$slug = $this->getSlug($request);
        $slug = "user-sites";

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));
        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];
        $searchable = $dataType->server_side ? array_keys(SchemaManager::describeTable(app($dataType->model_name)->getTable())->toArray()) : '';
        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', null);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + 1;
            $orderColumn = [[$index, 'desc']];
            if (!$sortOrder && isset($dataType->order_direction)) {
                $sortOrder = $dataType->order_direction;
                $orderColumn = [[$index, $dataType->order_direction]];
            } else {
                $orderColumn = [[$index, 'desc']];
            }
        }

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $query = $model->{$dataType->scope}();
            } else {
                $query = $model::select('*')->where('user_id',app('VoyagerAuth')->user()->id);
            }

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model)) && app('VoyagerAuth')->user()->can('delete', app($dataType->model_name))) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query = $query->withTrashed();
                }
            }

            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value != '' && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        if (($isModelTranslatable = is_bread_translatable($model))) {
            $dataTypeContent->load('translations');
        }

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->default_search_key ?? null;

        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::$slug.browse")) {
            $view = "voyager::$slug.browse";
        }
        return Voyager::view($view, compact(
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchable',
            'isServerSide',
            'defaultSearchKey',
            'usesSoftDeletes',
            'showSoftDeleted'
        ));
    }



    public function show(Request $request, $id)
    {

        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

	   //Checking if the user belongs to the data
        if(!DB::table($dataType->name)->where('id', $id)->where('user_id',app('VoyagerAuth')->user()->id)->exists()){

            return abort(404, 'Not Authorized');

        }

        $isSoftDeleted = false;

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
            if ($dataTypeContent->deleted_at) {
                $isSoftDeleted = true;
            }
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check permission
        $this->authorize('read', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.read';

        if (view()->exists("voyager::$slug.read")) {
            $view = "voyager::$slug.read";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'isSoftDeleted'));
    }


    public function edit(Request $request, $id)
    {


        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
			//Checking if the user belongs to the data
        if(!DB::table($dataType->name)->where('id', $id)->where('user_id',app('VoyagerAuth')->user()->id)->exists()){

            return abort(404, 'Not Authorized');

        }
        return redirect()
        ->route("voyager.{$dataType->slug}.index")
        ->with([
            'message'    => "You can edit site.",
            'alert-type' => 'error',
        ]);
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    // POST BR(E)AD
    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
			//Checking if the user belongs to the data
        if(!DB::table($dataType->name)->where('id', $id)->where('user_id',app('VoyagerAuth')->user()->id)->exists()){

            return abort(404, 'Not Authorized');
        }

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        }

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id)->validate();
        $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

        event(new BreadDataUpdated($dataType, $data));

        return redirect()
        ->route("voyager.{$dataType->slug}.index")
        ->with([
            'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
            'alert-type' => 'success',
        ]);
    }



    public function create(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0)
        ? new $dataType->model_name()
        : false;

        //Not Showing User Id in Create
        $dataType->addRows->forget(4);

        foreach ($dataType->addRows as $key => $row) {

            $dataType->addRows[$key]['col_width'] = $row->details->width ?? 100;
        }



        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    /**
     * POST BRE(A)D - Store data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

    	//Adding User Id For This Insertion
    	$request->request->add(['user_id' =>app('VoyagerAuth')->user()->id]);

    	
        $slug = $this->getSlug($request);



        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));


        // Validate fields with ajax
        $rules=array();
        $messages=array();
        $customAttributes=array();
        
        $rules["server_id"]="required";
        $rules["domain"]="required";
        $rules["project_type"]="required";
        $rules["username"]="required";

        $messages["server_id.required"]="Please select server";
        $messages["domain.required"]="Please enter domain name";
        $messages["project_type.required"]="Please select project type";
        $messages["username.required"]="Please enter user name";

        
        //$val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        /*if($validate->fails()){
            $validate->validate();
            //return response()->json(['errors' => $validate->messages()]);
            //var_dump($validate->messages());
        }*/

        $server = DB::table("user_apps")->where('id', $request->server_id)->where('user_id',app('VoyagerAuth')->user()->id)->first();

        if(!isset($server->id) || empty($server->server_id)){
            $rules["server_error"]="required";
            $messages["server_error.required"]="Server was not found";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            /*return redirect()
            ->route("voyager.{$dataType->slug}.create")
            ->with([
                //'message'    => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                'message'    => "server was not found",
                'alert-type' => 'error',
            ]);*/
        }
        $user_site_exits = DB::table("user_sites")->where('server_id', $request->server_id)->where('user_id',app('VoyagerAuth')->user()->id)->where('domain',$request->domain)->count();
        if($user_site_exits > 0){
            $rules["server_error"]="required";
            $messages["server_error.required"]=$request->doamin." has been already registered";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }
        try{
            $client = new Client();
            $headers=[
                'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                //'Accept'=>'application/json',
                //'Content-Type'=>'application/json'
            ];
            $body=[
                'domain' => $request->domain,
                'project_type' => "php",
                'username' => $request->username
            ];
            $res = $client->request('POST', 'https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites', [
                'form_params' => $body, 
                'headers' => $headers 
            ]);
            if ($res->getStatusCode() == 200) { // 200 OK
                //$response_data = $res->getBody()->getContents();
                //print_r($response_data);
                //exit;
                $response_data = $res->getBody()->getContents();
                $response_data=json_decode($response_data);
                //print_r($$response_data->site);exit;
                $request_data=array();
                if(isset($response_data->site->id)){
                    $request->request->add(['user_id' => app('VoyagerAuth')->user()->id]);
                    $request->request->add(['server_id' => $request->server_id]);
                    $request->request->add(['site_id' => $response_data->site->id]);
                    $request->request->add(['name' => $response_data->site->name]);
                    $request->request->add(['aliases' => json_encode($response_data->site->aliases)]);
                    $request->request->add(['directory' => $response_data->site->directory]);
                    $request->request->add(['wildcards' => $response_data->site->wildcards]);
                    $request->request->add(['site_status' => $response_data->site->status]);
                    $request->request->add(['repository' => $response_data->site->repository]);
                    $request->request->add(['repository_provider' => $response_data->site->repository_provider]);
                    $request->request->add(['repository_branch' => $response_data->site->repository_branch]);
                    $request->request->add(['repository_status' => $response_data->site->repository_status]);
                    $request->request->add(['quick_deploy' => 0]);
                    $request->request->add(['deployment_status' => $response_data->site->deployment_status]);
                    $request->request->add(['project_type' => $response_data->site->project_type]);
                    $request->request->add(['isolated' => 0]);
                    $request->request->add(['app' => $response_data->site->app]);
                    $request->request->add(['app_status' => $response_data->site->app_status]);
                    $request->request->add(['hipchat_room' => $response_data->site->hipchat_room]);
                    $request->request->add(['slack_channel' => $response_data->site->slack_channel]);
                    $request->request->add(['site_created_at' => $response_data->site->created_at]);
                    $request->request->add(['username' => $response_data->site->username]);
                    $request->request->add(['deployment_url' => $response_data->site->deployment_url]);
                }else{
                    $rules["server_error"]="required";
                    $messages["server_error.required"]="Something went to wrong with forge server because site detail is null";
                    $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
                }
                $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
                //var_dump($data);exit;
                event(new BreadDataAdded($dataType, $data));
                return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                    'message'    => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
            }else{
                $val = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }
        }catch(ClientException $e){
            $responseBodyAsString = $e->getResponse()->getBody()->getContents();
            $rules["server_error"]="required";
            $messages["server_error.required"]="Client exception : ".$responseBodyAsString;
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }catch(RequestException $e){
            /*print_r($e->getResponse());
            if ($e->hasResponse()) 
            echo Psr7\str($e->getResponse());exit;*/
            $rules["server_error"]="required";
            $messages["server_error.required"]="Request exception";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }catch(BadResponseException $e){
            /*print_r($e->getResponse());
            if ($e->hasResponse()) 
            echo GuzzleHttp\Psr7\str($e->getResponse());exit;*/
            //print_r($e->getResponse());exit;
            $rules["server_error"]="required";
            $messages["server_error.required"]="Bad response exception";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }catch(ServerException $e){
            /*print_r($e->getResponse());
            if ($e->hasResponse()) 
            echo GuzzleHttp\Psr7\str($e->getResponse());exit;*/

            $rules["server_error"]="required";
            $messages["server_error.required"]="Server exception";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();

        }catch(ConnectException $e){
            /*print_r($e->getResponse());
            if ($e->hasResponse()) 
            echo GuzzleHttp\Psr7\str($e->getResponse());exit;*/

            $rules["server_error"]="required";
            $messages["server_error.required"]="Connect exception";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }catch( TooManyRedirectsException $e){
            /*print_r($e->getResponse());
            if ($e->hasResponse()) 
            echo $e->getResponse();exit;*/

            $rules["server_error"]="required";
            $messages["server_error.required"]="TooMany redirects exception";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();

        }catch(Exception $e){
            $rules["server_error"]="required";
            $messages["server_error.required"]="Something wemt to wrong";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }catch(\Exception $e){
            $rules["server_error"]="required";
            $messages["server_error.required"]="Something wemt to wrong";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }
    }
    public function destroy(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
			//Checking if the user belongs to the data
        if(!DB::table($dataType->name)->where('id', $id)->where('user_id',app('VoyagerAuth')->user()->id)->exists()){

            return abort(404, 'Not Authorized');
        }
        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        // Init array of IDs
        $ids = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }
        foreach ($ids as $id) {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

            $model = app($dataType->model_name);
            if (!($model && in_array(SoftDeletes::class, class_uses($model)))) {
                $this->cleanup($dataType, $data);
            }
        }

        $displayName = count($ids) > 1 ? $dataType->display_name_plural : $dataType->display_name_singular;

        $res = $data->destroy($ids);
        $data = $res
        ? [
            'message'    => __('voyager::generic.successfully_deleted')." {$displayName}",
            'alert-type' => 'success',
        ]
        : [
            'message'    => __('voyager::generic.error_deleting')." {$displayName}",
            'alert-type' => 'error',
        ];

        if ($res) {
            event(new BreadDataDeleted($dataType, $data));
        }

        return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
    }

    public function restore(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
			//Checking if the user belongs to the data
        if(!DB::table($dataType->name)->where('id', $id)->where('user_id',app('VoyagerAuth')->user()->id)->exists()){

            return abort(404, 'Not Authorized');
        }
        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        // Get record
        $model = call_user_func([$dataType->model_name, 'withTrashed']);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        $data = $model->findOrFail($id);

        $displayName = $dataType->display_name_singular;

        $res = $data->restore($id);
        $data = $res
        ? [
            'message'    => __('voyager::generic.successfully_restored')." {$displayName}",
            'alert-type' => 'success',
        ]
        : [
            'message'    => __('voyager::generic.error_restoring')." {$displayName}",
            'alert-type' => 'error',
        ];

        if ($res) {
            event(new BreadDataRestored($dataType, $data));
        }

        return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
    }

    /**
     * Remove translations, images and files related to a BREAD item.
     *
     * @param \Illuminate\Database\Eloquent\Model $dataType
     * @param \Illuminate\Database\Eloquent\Model $data
     *
     * @return void
     */
    protected function cleanup($dataType, $data)
    {
        // Delete Translations, if present
        if (is_bread_translatable($data)) {
            $data->deleteAttributeTranslations($data->getTranslatableAttributes());
        }

        // Delete Images
        $this->deleteBreadImages($data, $dataType->deleteRows->where('type', 'image'));

        // Delete Files
        foreach ($dataType->deleteRows->where('type', 'file') as $row) {
            if (isset($data->{$row->field})) {
                foreach (json_decode($data->{$row->field}) as $file) {
                    $this->deleteFileIfExists($file->download_link);
                }
            }
        }

        // Delete media-picker files
        $dataType->rows->where('type', 'media_picker')->where('details.delete_files', true)->each(function ($row) use ($data) {
            $content = $data->{$row->field};
            if (isset($content)) {
                if (!is_array($content)) {
                    $content = json_decode($content);
                }
                if (is_array($content)) {
                    foreach ($content as $file) {
                        $this->deleteFileIfExists($file);
                    }
                } else {
                    $this->deleteFileIfExists($content);
                }
            }
        });
    }

    /**
     * Delete all images related to a BREAD item.
     *
     * @param \Illuminate\Database\Eloquent\Model $data
     * @param \Illuminate\Database\Eloquent\Model $rows
     *
     * @return void
     */
    public function deleteBreadImages($data, $rows)
    {
        foreach ($rows as $row) {
            if ($data->{$row->field} != config('voyager.user.default_avatar')) {
                $this->deleteFileIfExists($data->{$row->field});
            }

            if (isset($row->details->thumbnails)) {
                foreach ($row->details->thumbnails as $thumbnail) {
                    $ext = explode('.', $data->{$row->field});
                    $extension = '.'.$ext[count($ext) - 1];

                    $path = str_replace($extension, '', $data->{$row->field});

                    $thumb_name = $thumbnail->name;

                    $this->deleteFileIfExists($path.'-'.$thumb_name.$extension);
                }
            }
        }

        if ($rows->count() > 0) {
            event(new BreadImagesDeleted($data, $rows));
        }
    }

    /**
     * Order BREAD items.
     *
     * @param string $table
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function order(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        if (!isset($dataType->order_column) || !isset($dataType->order_display_column)) {
            return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message'    => __('voyager::bread.ordering_not_set'),
                'alert-type' => 'error',
            ]);
        }

        $model = app($dataType->model_name);
        if ($model && in_array(SoftDeletes::class, class_uses($model))) {
            $model = $model->withTrashed();
        }
        $results = $model->orderBy($dataType->order_column, $dataType->order_direction)->get();

        $display_column = $dataType->order_display_column;

        $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->whereField($display_column)->first();

        $view = 'voyager::bread.order';

        if (view()->exists("voyager::$slug.order")) {
            $view = "voyager::$slug.order";
        }

        return Voyager::view($view, compact(
            'dataType',
            'display_column',
            'dataRow',
            'results'
        ));
    }

    public function update_order(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        $model = app($dataType->model_name);

        $order = json_decode($request->input('order'));
        $column = $dataType->order_column;
        foreach ($order as $key => $item) {
            if ($model && in_array(SoftDeletes::class, class_uses($model))) {
                $i = $model->withTrashed()->findOrFail($item->id);
            } else {
                $i = $model->findOrFail($item->id);
            }
            $i->$column = ($key + 1);
            $i->save();
        }
    }

    public function action(Request $request)
    {
        $slug = $this->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $action = new $request->action($dataType, null);
        return $action->massAction(explode(',', $request->ids), $request->headers->get('referer'));
    }

    /**
     * Get BREAD relations data.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function relation(Request $request)
    {
        $slug = $this->getSlug($request);
        $page = $request->input('page');
        $on_page = 50;
        $search = $request->input('search', false);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        foreach ($dataType->editRows as $key => $row) {
            if ($row->field === $request->input('type')) {
                $options = $row->details;
                $skip = $on_page * ($page - 1);

                // If search query, use LIKE to filter results depending on field label
                if ($search) {
                    $total_count = app($options->model)->where($options->label, 'LIKE', '%'.$search.'%')->count();
                    $relationshipOptions = app($options->model)->take($on_page)->skip($skip)
                    ->where($options->label, 'LIKE', '%'.$search.'%')
                    ->get();
                } else {
                    $total_count = app($options->model)->count();
                    $relationshipOptions = app($options->model)->take($on_page)->skip($skip)->get();
                }

                $results = [];
                foreach ($relationshipOptions as $relationshipOption) {
                    $results[] = [
                        'id'   => $relationshipOption->{$options->key},
                        'text' => $relationshipOption->{$options->label},
                    ];
                }

                return response()->json([
                    'results'    => $results,
                    'pagination' => [
                        'more' => ($total_count > ($skip + $on_page)),
                    ],
                ]);
            }
        }

        // No result found, return empty array
        return response()->json([], 404);
    }


    public function deployView(Request $request,$id)
    {
        $viewType="deploy";

        $slug = 'user-sites';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
            //Checking if the user belongs to the data
        if(!DB::table($dataType->name)->where('id', $id)->where('user_id',app('VoyagerAuth')->user()->id)->exists()){

            return abort(404, 'Not Authorized');

        }
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable','viewType'));
    }

    
    public function deploy(Request $request,$id)
    {

        $slug = 'user-sites';
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
            //Checking if the user belongs to the data
        if(!DB::table($dataType->name)->where('id', $id)->where('user_id',app('VoyagerAuth')->user()->id)->exists()){

            return abort(404, 'Not Authorized');
        }

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        }

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $rules=array();
        $messages=array();
        $customAttributes=array();
        
        $rules["repository"]="required";
        $rules["repository_provider"]="required";
        $rules["branch"]="required";

        $messages["repository.required"]="Please enter repository";
        $messages["repository_provider.required"]="Please select repository provider";
        $messages["branch.required"]="Please enter branch";

        //$val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        $server = DB::table("user_apps")->where('id', $dataTypeContent->server_id)->where('user_id',app('VoyagerAuth')->user()->id)->first();
        if(!isset($server->id) || empty($server->server_id)){
            $rules["server_error"]="required";
            $messages["server_error.required"]="Server was not found";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }
        if($dataTypeContent->site_status != "installed"){
            try{
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                        //'Accept'=>'application/json',
                        //'Content-Type'=>'application/json'
                ];
                $body=[];
                $res = $client->request('GET', 'https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);
                if ($res->getStatusCode() == 200) { // 200 OK
                    $response_data = $res->getBody()->getContents();
                    $response_data=json_decode($response_data);
                    $request_data=array();

                    if(isset($response_data->sites) && count($response_data->sites) > 0){
                        $found=false;
                        foreach ($response_data->sites as $site) {
                            //echo $site->id."-".$site->status;
                            if($site->id == $dataTypeContent->site_id){
                                if($site->status == "installed"){
                                    $found=true;
                                }
                                DB::table('user_sites')->where('id', $dataTypeContent->id)->update(['site_status' => $site->status]);
                            }

                        }
                        if(!$found){
                            $rules["server_error"]="required";
                            $messages["server_error.required"]="Site was not installed";
                            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();    
                        }else{
                            try{
                                $client = new Client();
                                $headers=[
                                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                                    //'Accept'=>'application/json',
                                    //'Content-Type'=>'application/json'
                                ];
                                $body=[
                                    'repository'=>$request->repository,
                                    'provider'=>$request->repository_provider,
                                    'branch'=>$request->branch
                                ];
                                $res = $client->request('POST','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$dataTypeContent->site_id.'/git', [
                                    'form_params' => $body, 
                                    'headers' => $headers 
                                ]);
                                //print_r($res);exit;
                                //echo $res->getStatusCode();exit;
                                if ($res->getStatusCode() == 200) { // 200 OK
                                        //$response_data = $res->getBody()->getContents();
                                        //$response_data=json_decode($response_data);
                                        //print_r($res->getBody());exit;
                                    //$this->insertUpdateData($request, $slug, $dataType->editRows, $data);
                                    DB::table('user_sites')->where('id', $dataTypeContent->id)->update(['repository' => $request->repository,'repository_provider'=>$request->repository_provider,'repository_branch'=>$request->branch]);
                                    event(new BreadDataUpdated($dataType, $data));
                                    return redirect()
                                    ->route("voyager.{$dataType->slug}.index")
                                    ->with([
                                        'message'    => "Deployment saved success and please wait untill it finished",
                                        'alert-type' => 'success',
                                    ]);
                                }
                            }catch(ClientException $e){
                                $responseBodyAsString = $e->getResponse()->getBody()->getContents();
                                $rules["server_error"]="required";
                                $messages["server_error.required"]="Client exception : ".$responseBodyAsString;
                                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
                            }catch(RequestException $e){
                                $rules["server_error"]="required";
                                $messages["server_error.required"]="Request exception";
                                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
                            }catch(BadResponseException $e){
                                $rules["server_error"]="required";
                                $messages["server_error.required"]="Bad response exception";
                                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
                            }catch(ServerException $e){
                                $rules["server_error"]="required";
                                $messages["server_error.required"]="Server exception";
                                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
                            }catch(ConnectException $e){
                                $rules["server_error"]="required";
                                $messages["server_error.required"]="Connect exception";
                                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
                            }catch( TooManyRedirectsException $e){
                                $rules["server_error"]="required";
                                $messages["server_error.required"]="TooMany redirects exception";
                                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
                            }catch(Exception $e){
                                $rules["server_error"]="required";
                                $messages["server_error.required"]="Something wemt to wrong";
                                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
                            }catch(\Exception $e){
                                $rules["server_error"]="required";
                                $messages["server_error.required"]="Something wemt to wrong";
                                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
                            }
                        }
                    }else{
                        $rules["server_error"]="required";
                        $messages["server_error.required"]="Site was not found on server.";
                        $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
                    }
                    $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
                        //var_dump($data);exit;
                    event(new BreadDataAdded($dataType, $data));
                    return redirect()
                    ->route("voyager.{$dataType->slug}.index")
                    ->with([
                        'message'    => "Deployment saved success and please wait untill it finished",
                        'alert-type' => 'success',
                    ]);
                }else{
                    $rules["server_error"]="required";
                    $messages["server_error.required"]="Cna not fetch detail from server.";
                    $val = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
                }
            }catch(ClientException $e){
                $responseBodyAsString = $e->getResponse()->getBody()->getContents();
                $rules["server_error"]="required";
                $messages["server_error.required"]="Client exception : ".$responseBodyAsString;
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(RequestException $e){

                $rules["server_error"]="required";
                $messages["server_error.required"]="Request exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(BadResponseException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Bad response exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(ServerException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Server exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(ConnectException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Connect exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch( TooManyRedirectsException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="TooMany redirects exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(Exception $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Something wemt to wrong";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(\Exception $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Something wemt to wrong";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }
            $rules["server_error"]="required";
            $messages["server_error.required"]="Site is not installed";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();   
        }else{
            try{
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                    //'Accept'=>'application/json',
                    //'Content-Type'=>'application/json'
                ];
                $body=[
                    'repository'=>$request->repository,
                    'provider'=>$request->repository_provider,
                    'branch'=>$request->branch
                ];
                $res = $client->request('POST','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$dataTypeContent->site_id.'/git', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);

                if ($res->getStatusCode() == 200) { // 200 OK
                    //$this->insertUpdateData($request, $slug, $dataType->editRows, $data);
                    DB::table('user_sites')->where('id', $dataTypeContent->id)->update(['repository' => $request->repository,'repository_provider'=>$request->repository_provider,'repository_branch'=>$request->branch]);
                    event(new BreadDataUpdated($dataType, $data));
                    return redirect()
                    ->route("voyager.{$dataType->slug}.index")
                    ->with([
                        'message'    =>"Deployment saved success and please wait untill it finished",
                        'alert-type' => 'success',
                    ]);
                }
            }catch(ClientException $e){
                $responseBodyAsString = $e->getResponse()->getBody()->getContents();
                $rules["server_error"]="required";
                $messages["server_error.required"]="Client exception : ".$responseBodyAsString;
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(RequestException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Request exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(BadResponseException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Bad response exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(ServerException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Server exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(ConnectException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Connect exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch( TooManyRedirectsException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="TooMany redirects exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(Exception $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Something wemt to wrong";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(\Exception $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Something wemt to wrong";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }
        }
    }

    public function deployScriptView(Request $request,$id)
    {
        $customAttributes=array();
        $viewType="deployScript";

        $slug = 'user-sites';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
            //Checking if the user belongs to the data
        if(!DB::table($dataType->name)->where('id', $id)->where('user_id',app('VoyagerAuth')->user()->id)->exists()){

            return abort(404, 'Not Authorized');

        }
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        /*Get script from server*/
        if(empty($dataTypeContent->deployment_script)){

            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
            $server = DB::table("user_apps")->where('id', $dataTypeContent->server_id)->where('user_id',app('VoyagerAuth')->user()->id)->first();
            if(!isset($server->id) || empty($server->server_id)){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Server was not found";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }
            if($dataTypeContent->site_status != "installed"){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Site is not installed";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();   
            }
            try{
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                    //'Accept'=>'application/json',
                    //'Content-Type'=>'application/json'
                ];
                $body=[];
                $res = $client->request('GET','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$dataTypeContent->site_id.'/deployment/script', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);
                if ($res->getStatusCode() == 200) { // 200 OK
                    $response_data = $res->getBody()->getContents();
                    if(!empty($response_data)){
                        $dataTypeContent->deployment_script=trim($response_data);
                    }
                }
                
            }catch(ClientException $e){
                $responseBodyAsString = $e->getResponse()->getBody()->getContents();
                $rules["server_error"]="required";
                $messages["server_error.required"]="Client exception : ".$responseBodyAsString;
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(RequestException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Request exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(BadResponseException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Bad response exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(ServerException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Server exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(ConnectException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Connect exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch( TooManyRedirectsException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="TooMany redirects exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(Exception $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Something wemt to wrong";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(\Exception $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Something wemt to wrong";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }
        }

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable','viewType'));
    }

    public function deployScript(Request $request,$id)
    {

        $slug = 'user-sites';
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
            //Checking if the user belongs to the data
        if(!DB::table($dataType->name)->where('id', $id)->where('user_id',app('VoyagerAuth')->user()->id)->exists()){

            return abort(404, 'Not Authorized');
        }

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        }

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $rules=array();
        $messages=array();
        $customAttributes=array();
        
        $rules["deployment_script"]="required";
        $messages["deployment_script.required"]="Please enter deploy script";

        $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        $server = DB::table("user_apps")->where('id', $dataTypeContent->server_id)->where('user_id',app('VoyagerAuth')->user()->id)->first();
        if(!isset($server->id) || empty($server->server_id)){
            $rules["server_error"]="required";
            $messages["server_error.required"]="Server was not found";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }
        if($dataTypeContent->site_status != "installed"){
            $rules["server_error"]="required";
            $messages["server_error.required"]="Site is not installed";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();   
        }else{
            try{
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                    //'Accept'=>'application/json',
                    //'Content-Type'=>'application/json'
                ];
                $body=[
                    'content'=>$request->deployment_script,
                ];
                $res = $client->request('PUT','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$dataTypeContent->site_id.'/deployment/script', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);
                if ($res->getStatusCode() == 200) { // 200 OK
                    DB::table('user_sites')->where('id', $dataTypeContent->id)->update(['deployment_script' => $request->deployment_script]);
                    event(new BreadDataUpdated($dataType, $data));
                    return redirect()
                    ->route("voyager.{$dataType->slug}.index")
                    ->with([
                        'message'    =>'Deployment script updated.',
                        'alert-type' => 'success',
                    ]);
                }else{
                    $rules["server_error"]="required";
                    $messages["server_error.required"]="Can not update script to server";
                    $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();    
                }
                
            }catch(ClientException $e){
                $responseBodyAsString = $e->getResponse()->getBody()->getContents();
                $rules["server_error"]="required";
                $messages["server_error.required"]="Client exception : ".$responseBodyAsString;
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(RequestException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Request exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(BadResponseException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Bad response exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(ServerException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Server exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(ConnectException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Connect exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch( TooManyRedirectsException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="TooMany redirects exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(Exception $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Something wemt to wrong";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(\Exception $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Something wemt to wrong";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }
        }
    }

    public function deployNow(Request $request,$id)
    {

        $customAttributes=array();
        $slug = 'user-sites';
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
            //Checking if the user belongs to the data
        if(!DB::table($dataType->name)->where('id', $id)->where('user_id',app('VoyagerAuth')->user()->id)->exists()){

            return abort(404, 'Not Authorized');
        }

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        }

        // Check permission
        $this->authorize('edit', $data);

        $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        $server = DB::table("user_apps")->where('id', $dataTypeContent->server_id)->where('user_id',app('VoyagerAuth')->user()->id)->first();
        if(!isset($server->id) || empty($server->server_id)){
            $rules["server_error"]="required";
            $messages["server_error.required"]="Server was not found";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }
        if($dataTypeContent->site_status != "installed"){
            $rules["server_error"]="required";
            $messages["server_error.required"]="Site is not installed";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();   
        }else{
            try{
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                    //'Accept'=>'application/json',
                    //'Content-Type'=>'application/json'
                ];
                $body=[];
                $res = $client->request('POST','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$dataTypeContent->site_id.'/deployment/deploy', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);
                if ($res->getStatusCode() == 200) { // 200 OK
                    return redirect()
                    ->route("voyager.{$dataType->slug}.index")
                    ->with([
                        'message'    =>'Deployment Now Excecuted',
                        'alert-type' => 'success',
                    ]);
                }else{
                    $rules["server_error"]="required";
                    $messages["server_error.required"]="Failed to Deploy Now";
                    $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();    
                }
                
            }catch(ClientException $e){
                $responseBodyAsString = $e->getResponse()->getBody()->getContents();
                $rules["server_error"]="required";
                $messages["server_error.required"]="Client exception : ".$responseBodyAsString;
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(RequestException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Request exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(BadResponseException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Bad response exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(ServerException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Server exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(ConnectException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Connect exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch( TooManyRedirectsException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="TooMany redirects exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(Exception $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Something wemt to wrong";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(\Exception $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Something wemt to wrong";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }
        }
    }
    
    public function deployScriptFromServer(Request $request,$id)
    {

        $slug = 'user-sites';
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
            //Checking if the user belongs to the data
        if(!DB::table($dataType->name)->where('id', $id)->where('user_id',app('VoyagerAuth')->user()->id)->exists()){

            return abort(404, 'Not Authorized');
        }

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        }

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $rules=array();
        $messages=array();
        $customAttributes=array();
        
        $rules["repository"]="required";
        $rules["repository_provider"]="required";
        $rules["branch"]="required";

        $messages["repository.required"]="Please enter repository";
        $messages["repository_provider.required"]="Please select repository provider";
        $messages["branch.required"]="Please enter branch";

        //$val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        $server = DB::table("user_apps")->where('id', $dataTypeContent->server_id)->where('user_id',app('VoyagerAuth')->user()->id)->first();
        if(!isset($server->id) || empty($server->server_id)){
            $rules["server_error"]="required";
            $messages["server_error.required"]="Server was not found";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }
        try{
            $client = new Client();
            $headers=[
                'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                //'Accept'=>'application/json',
                //'Content-Type'=>'application/json'
            ];
            $body=[];
            $res = $client->request('GET','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$dataTypeContent->site_id.'/deployment/script', [
                'form_params' => $body, 
                'headers' => $headers 
            ]);
            //print_r($res);exit;
            //echo $res->getStatusCode();exit;
            if ($res->getStatusCode() == 200) { // 200 OK
                //$response_data = $res->getBody()->getContents();
                //$response_data=json_decode($response_data);
                //print_r($res->getBody());exit;
            }
        }catch(ClientException $e){
            $responseBodyAsString = $e->getResponse()->getBody()->getContents();
            $rules["server_error"]="required";
            $messages["server_error.required"]="Client exception : ".$responseBodyAsString;
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }catch(RequestException $e){
            $rules["server_error"]="required";
            $messages["server_error.required"]="Request exception";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }catch(BadResponseException $e){
            $rules["server_error"]="required";
            $messages["server_error.required"]="Bad response exception";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }catch(ServerException $e){
            $rules["server_error"]="required";
            $messages["server_error.required"]="Server exception";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }catch(ConnectException $e){
            $rules["server_error"]="required";
            $messages["server_error.required"]="Connect exception";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }catch( TooManyRedirectsException $e){
            $rules["server_error"]="required";
            $messages["server_error.required"]="TooMany redirects exception";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }catch(Exception $e){
            $rules["server_error"]="required";
            $messages["server_error.required"]="Something wemt to wrong";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }catch(\Exception $e){
            $rules["server_error"]="required";
            $messages["server_error.required"]="Something wemt to wrong";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }
        if($dataTypeContent->site_status != "installed"){
            try{
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                        //'Accept'=>'application/json',
                        //'Content-Type'=>'application/json'
                ];
                $body=[];
                $res = $client->request('GET', 'https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);
                if ($res->getStatusCode() == 200) { // 200 OK
                    $response_data = $res->getBody()->getContents();
                    $response_data=json_decode($response_data);
                    $request_data=array();

                    if(isset($response_data->sites) && count($response_data->sites) > 0){
                        $found=false;
                        foreach ($response_data->sites as $site) {
                            //echo $site->id."-".$site->status;
                            if($site->id == $dataTypeContent->site_id){
                                if($site->status == "installed"){
                                    $found=true;
                                }
                                DB::table('user_sites')->where('id', $dataTypeContent->id)->update(['site_status' => $site->status]);
                            }

                        }
                        if(!$found){
                            $rules["server_error"]="required";
                            $messages["server_error.required"]="Site was not installed";
                            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();    
                        }else{
                            try{
                                $client = new Client();
                                $headers=[
                                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                                    //'Accept'=>'application/json',
                                    //'Content-Type'=>'application/json'
                                ];
                                $body=[
                                    'repository'=>$request->repository,
                                    'provider'=>$request->repository_provider,
                                    'branch'=>$request->branch
                                ];
                                $res = $client->request('POST','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$dataTypeContent->site_id.'/git', [
                                    'form_params' => $body, 
                                    'headers' => $headers 
                                ]);
                                //print_r($res);exit;
                                //echo $res->getStatusCode();exit;
                                if ($res->getStatusCode() == 200) { // 200 OK
                                        //$response_data = $res->getBody()->getContents();
                                        //$response_data=json_decode($response_data);
                                        //print_r($res->getBody());exit;
                                    //$this->insertUpdateData($request, $slug, $dataType->editRows, $data);
                                    DB::table('user_sites')->where('id', $dataTypeContent->id)->update(['repository' => $request->repository,'repository_provider'=>$request->repository_provider,'repository_branch'=>$request->branch]);
                                    event(new BreadDataUpdated($dataType, $data));
                                    return redirect()
                                    ->route("voyager.{$dataType->slug}.index")
                                    ->with([
                                        'message'    => "Deployment saved success and please wait untill it finished",
                                        'alert-type' => 'success',
                                    ]);
                                }
                            }catch(ClientException $e){
                                $responseBodyAsString = $e->getResponse()->getBody()->getContents();
                                $rules["server_error"]="required";
                                $messages["server_error.required"]="Client exception : ".$responseBodyAsString;
                                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
                            }catch(RequestException $e){
                                $rules["server_error"]="required";
                                $messages["server_error.required"]="Request exception";
                                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
                            }catch(BadResponseException $e){
                                $rules["server_error"]="required";
                                $messages["server_error.required"]="Bad response exception";
                                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
                            }catch(ServerException $e){
                                $rules["server_error"]="required";
                                $messages["server_error.required"]="Server exception";
                                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
                            }catch(ConnectException $e){
                                $rules["server_error"]="required";
                                $messages["server_error.required"]="Connect exception";
                                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
                            }catch( TooManyRedirectsException $e){
                                $rules["server_error"]="required";
                                $messages["server_error.required"]="TooMany redirects exception";
                                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
                            }catch(Exception $e){
                                $rules["server_error"]="required";
                                $messages["server_error.required"]="Something wemt to wrong";
                                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
                            }catch(\Exception $e){
                                $rules["server_error"]="required";
                                $messages["server_error.required"]="Something wemt to wrong";
                                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
                            }
                        }
                    }else{
                        $rules["server_error"]="required";
                        $messages["server_error.required"]="Site was not found on server.";
                        $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
                    }
                    $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
                        //var_dump($data);exit;
                    event(new BreadDataAdded($dataType, $data));
                    return redirect()
                    ->route("voyager.{$dataType->slug}.index")
                    ->with([
                        'message'    => "Deployment saved success and please wait untill it finished",
                        'alert-type' => 'success',
                    ]);
                }else{
                    $rules["server_error"]="required";
                    $messages["server_error.required"]="Cna not fetch detail from server.";
                    $val = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
                }
            }catch(ClientException $e){
                $responseBodyAsString = $e->getResponse()->getBody()->getContents();
                $rules["server_error"]="required";
                $messages["server_error.required"]="Client exception : ".$responseBodyAsString;
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(RequestException $e){

                $rules["server_error"]="required";
                $messages["server_error.required"]="Request exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(BadResponseException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Bad response exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(ServerException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Server exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(ConnectException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Connect exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch( TooManyRedirectsException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="TooMany redirects exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(Exception $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Something wemt to wrong";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(\Exception $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Something wemt to wrong";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }
            $rules["server_error"]="required";
            $messages["server_error.required"]="Site is not installed";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();   
        }else{
            try{
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                    //'Accept'=>'application/json',
                    //'Content-Type'=>'application/json'
                ];
                $body=[
                    'repository'=>$request->repository,
                    'provider'=>$request->repository_provider,
                    'branch'=>$request->branch
                ];
                $res = $client->request('POST','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$dataTypeContent->site_id.'/git', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);

                if ($res->getStatusCode() == 200) { // 200 OK
                    //$this->insertUpdateData($request, $slug, $dataType->editRows, $data);
                    DB::table('user_sites')->where('id', $dataTypeContent->id)->update(['repository' => $request->repository,'repository_provider'=>$request->repository_provider,'repository_branch'=>$request->branch]);
                    event(new BreadDataUpdated($dataType, $data));
                    return redirect()
                    ->route("voyager.{$dataType->slug}.index")
                    ->with([
                        'message'    =>"Deployment saved success and please wait untill it finished",
                        'alert-type' => 'success',
                    ]);
                }
            }catch(ClientException $e){
                $responseBodyAsString = $e->getResponse()->getBody()->getContents();
                $rules["server_error"]="required";
                $messages["server_error.required"]="Client exception : ".$responseBodyAsString;
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(RequestException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Request exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(BadResponseException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Bad response exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(ServerException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Server exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(ConnectException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Connect exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch( TooManyRedirectsException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="TooMany redirects exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(Exception $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Something wemt to wrong";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(\Exception $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Something wemt to wrong";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }
        }
    }

    public function envView(Request $request,$id)
    {
        $customAttributes=array();
        $viewType="env";

        $slug = 'user-sites';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
            //Checking if the user belongs to the data
        if(!DB::table($dataType->name)->where('id', $id)->where('user_id',app('VoyagerAuth')->user()->id)->exists()){

            return abort(404, 'Not Authorized');

        }
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        /*Get script from server*/
        if(empty($dataTypeContent->env_file)){

            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
            $server = DB::table("user_apps")->where('id', $dataTypeContent->server_id)->where('user_id',app('VoyagerAuth')->user()->id)->first();
            if(!isset($server->id) || empty($server->server_id)){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Server was not found";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }
            if($dataTypeContent->site_status != "installed"){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Site is not installed";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();   
            }
            try{
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                    //'Accept'=>'application/json',
                    //'Content-Type'=>'application/json'
                ];
                $body=[];
                $res = $client->request('GET','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$dataTypeContent->site_id.'/env', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);
                if ($res->getStatusCode() == 200) { // 200 OK
                    $response_data = $res->getBody()->getContents();
                    if(!empty($response_data)){
                        $dataTypeContent->env_file=trim($response_data);
                    }
                }
                
            }catch(ClientException $e){
                $responseBodyAsString = $e->getResponse()->getBody()->getContents();
                $rules["server_error"]="required";
                $messages["server_error.required"]="Client exception : ".$responseBodyAsString;
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(RequestException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Request exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(BadResponseException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Bad response exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(ServerException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Server exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(ConnectException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Connect exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch( TooManyRedirectsException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="TooMany redirects exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(Exception $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Something wemt to wrong";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(\Exception $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Something wemt to wrong";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }
        }

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable','viewType'));
    }

    public function envUpdate(Request $request,$id)
    {

        $slug = 'user-sites';
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
            //Checking if the user belongs to the data
        if(!DB::table($dataType->name)->where('id', $id)->where('user_id',app('VoyagerAuth')->user()->id)->exists()){

            return abort(404, 'Not Authorized');
        }

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        }

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $rules=array();
        $messages=array();
        $customAttributes=array();
        
        $rules["env_file"]="required";
        $messages["env_file.required"]="Please enter env script";

        $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        $server = DB::table("user_apps")->where('id', $dataTypeContent->server_id)->where('user_id',app('VoyagerAuth')->user()->id)->first();
        if(!isset($server->id) || empty($server->server_id)){
            $rules["server_error"]="required";
            $messages["server_error.required"]="Server was not found";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }
        if($dataTypeContent->site_status != "installed"){
            $rules["server_error"]="required";
            $messages["server_error.required"]="Site is not installed";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();   
        }else{
            try{
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                    //'Accept'=>'application/json',
                    //'Content-Type'=>'application/json'
                ];
                $body=[
                    'content'=>$request->env_file,
                ];
                $res = $client->request('PUT','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$dataTypeContent->site_id.'/env', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);
                if ($res->getStatusCode() == 200) { // 200 OK
                    DB::table('user_sites')->where('id', $dataTypeContent->id)->update(['env_file' => $request->env_file]);
                    event(new BreadDataUpdated($dataType, $data));
                    return redirect()
                    ->route("voyager.{$dataType->slug}.index")
                    ->with([
                        'message'    =>'Env file updated successfully.',
                        'alert-type' => 'success',
                    ]);
                }else{
                    $rules["server_error"]="required";
                    $messages["server_error.required"]="Can not update env file to server";
                    $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();    
                }
                
            }catch(ClientException $e){
                $responseBodyAsString = $e->getResponse()->getBody()->getContents();
                $rules["server_error"]="required";
                $messages["server_error.required"]="Client exception : ".$responseBodyAsString;
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(RequestException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Request exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(BadResponseException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Bad response exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(ServerException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Server exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(ConnectException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Connect exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch( TooManyRedirectsException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="TooMany redirects exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(Exception $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Something wemt to wrong";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }catch(\Exception $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Something wemt to wrong";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }
        }
    }

    public function createAjax(Request $request)
    {

        //Adding User Id For This Insertion
        $request->request->add(['user_id' =>app('VoyagerAuth')->user()->id]);
        $slug = 'user-sites';
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $rules=array();
        $messages=array();
        $customAttributes=array();
        
        $rules["server_id"]="required";
        $rules["domain"]="required";
        $rules["project_type"]="required";
        $rules["username"]="required";

        $messages["server_id.required"]="Please select server";
        $messages["domain.required"]="Please enter domain name";
        $messages["project_type.required"]="Please select project type";
        $messages["username.required"]="Please enter user name";

        
        //$val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        //$validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        $validate = Validator::make($request->all(), $rules, $messages, $customAttributes);
        if($validate->fails()){
            //$validate->validate();
            //return response()->json(['errors' => $validate->messages()]);
            //var_dump($validate->messages());
            foreach ($validate->errors()->all() as $error) {
                return response()->json(['message' => $error,'status'=>false]);exit;
            }
        }

        $server = DB::table("user_apps")->where('id', $request->server_id)->where('user_id',app('VoyagerAuth')->user()->id)->first();

        if(!isset($server->id) || empty($server->server_id)){
            return response()->json(['message' => "Server was not found",'status'=>false]);exit;
        }
        $user_site_exits = DB::table("user_sites")->where('server_id', $request->server_id)->where('user_id',app('VoyagerAuth')->user()->id)->where('domain',$request->domain)->count();
        if($user_site_exits > 0){
            return response()->json(['message' => $request->doamin." has been already registered",'status'=>false]);exit;
        }
        try{
            //remove
            //return response()->json(['message' => "status",'status'=>true,'id'=>16]);exit;
            $client = new Client();
            $headers=[
                'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                //'Accept'=>'application/json',
                //'Content-Type'=>'application/json'
            ];
            $body=[
                'domain' => $request->domain,
                'project_type' => "php",
                'username' => $request->username
            ];
            $res = $client->request('POST', 'https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites', [
                'form_params' => $body, 
                'headers' => $headers 
            ]);
            if ($res->getStatusCode() == 200) { // 200 OK
                //$response_data = $res->getBody()->getContents();
                //print_r($response_data);
                //exit;
                $response_data = $res->getBody()->getContents();
                $response_data=json_decode($response_data);
                //print_r($$response_data->site);exit;
                $request_data=array();
                if(isset($response_data->site->id)){
                    $request->request->add(['user_id' => app('VoyagerAuth')->user()->id]);
                    $request->request->add(['server_id' => $request->server_id]);
                    $request->request->add(['site_id' => $response_data->site->id]);
                    $request->request->add(['name' => $response_data->site->name]);
                    $request->request->add(['aliases' => json_encode($response_data->site->aliases)]);
                    $request->request->add(['directory' => $response_data->site->directory]);
                    $request->request->add(['wildcards' => $response_data->site->wildcards]);
                    $request->request->add(['site_status' => $response_data->site->status]);
                    $request->request->add(['repository' => $response_data->site->repository]);
                    $request->request->add(['repository_provider' => $response_data->site->repository_provider]);
                    $request->request->add(['repository_branch' => $response_data->site->repository_branch]);
                    $request->request->add(['repository_status' => $response_data->site->repository_status]);
                    $request->request->add(['quick_deploy' => 0]);
                    $request->request->add(['deployment_status' => $response_data->site->deployment_status]);
                    $request->request->add(['project_type' => $response_data->site->project_type]);
                    $request->request->add(['isolated' => 0]);
                    $request->request->add(['app' => $response_data->site->app]);
                    $request->request->add(['app_status' => $response_data->site->app_status]);
                    //$request->request->add(['hipchat_room' => $response_data->site->hipchat_room]);
                    $request->request->add(['slack_channel' => $response_data->site->slack_channel]);
                    $request->request->add(['site_created_at' => $response_data->site->created_at]);
                    $request->request->add(['username' => $response_data->site->username]);
                    $request->request->add(['deployment_url' => $response_data->site->deployment_url]);
                }else{
                    return response()->json(['message' => "Something went to wrong with forge server because site detail is null",'status'=>false]);exit;
                }
                $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
                //var_dump($data);exit;
                event(new BreadDataAdded($dataType, $data));
                return response()->json(['message' => "Site created successfully",'status'=>true,'id'=>$data->id]);exit;
            }else{
                return response()->json(['message' => "Something went to wrong with forge server because site detail is null",'status'=>false]);exit;
            }
        }catch(ClientException $e){
            $responseBodyAsString = $e->getResponse()->getBody()->getContents();
            return response()->json(['message' => "Client exception : ".$responseBodyAsString,'status'=>false]);exit;
        }catch(RequestException $e){
            return response()->json(['message' =>"Request exception",'status'=>false]);exit;
        }catch(BadResponseException $e){
            return response()->json(['message' =>"Bad response exception",'status'=>false]);exit;
        }catch(ServerException $e){
            return response()->json(['message' =>"Server exception",'status'=>false]);exit;
        }catch(ConnectException $e){
            return response()->json(['message' =>"Connect exception",'status'=>false]);exit;
        }catch( TooManyRedirectsException $e){
            return response()->json(['message' =>"TooMany redirects exception",'status'=>false]);exit;
        }catch(Exception $e){
            return response()->json(['message' =>"Something wemt to wrong",'status'=>false]);exit;
        }catch(\Exception $e){
            return response()->json(['message' =>"Something wemt to wrong",'status'=>false]);exit;
        }
    }

    public function repoAjax(Request $request){
        if(!$request->has("id")){
            return response()->json(['message' => "Site was not found",'status'=>false]);exit;
        }
        $id=$request->get("id");
        $slug = 'user-sites';
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
            //Checking if the user belongs to the data
        if(!DB::table($dataType->name)->where('id', $id)->where('user_id',app('VoyagerAuth')->user()->id)->exists()){

            return abort(404, 'Not Authorized');
        }

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        }

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $rules=array();
        $messages=array();
        $customAttributes=array();
        
        $rules["repository"]="required";
        $rules["repository_provider"]="required";
        $rules["branch"]="required";

        $messages["repository.required"]="Please enter repository";
        $messages["repository_provider.required"]="Please select repository provider";
        $messages["branch.required"]="Please enter branch";

        //$val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $validate = Validator::make($request->all(), $rules, $messages, $customAttributes);
        if($validate->fails()){
            foreach ($validate->errors()->all() as $error) {
                return response()->json(['message' => $error,'status'=>false]);exit;
            }
        }
        $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        $server = DB::table("user_apps")->where('id', $dataTypeContent->server_id)->where('user_id',app('VoyagerAuth')->user()->id)->first();
        if(!isset($server->id) || empty($server->server_id)){
            return response()->json(['message' => "Server was not found","status"=>false]);exit;
        }
        if($dataTypeContent->site_status != "installed"){
            try{
                //remove
                //return response()->json(['message' => "installed successfully","status"=>true]);exit;    
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                        //'Accept'=>'application/json',
                        //'Content-Type'=>'application/json'
                ];
                $body=[];
                $res = $client->request('GET', 'https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);
                if ($res->getStatusCode() == 200) { // 200 OK
                    $response_data = $res->getBody()->getContents();
                    $response_data=json_decode($response_data);
                    $request_data=array();

                    if(isset($response_data->sites) && count($response_data->sites) > 0){
                        $found=false;
                        foreach ($response_data->sites as $site) {
                            //echo $site->id."-".$site->status;
                            if($site->id == $dataTypeContent->site_id){
                                if($site->status == "installed"){
                                    $found=true;
                                }
                                DB::table('user_sites')->where('id', $dataTypeContent->id)->update(['site_status' => $site->status]);
                            }

                        }
                        if(!$found){
                            return response()->json(['message' => "Site was not installed","status"=>false]);exit;    
                        }else{
                            try{
                                $client = new Client();
                                $headers=[
                                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                                    //'Accept'=>'application/json',
                                    //'Content-Type'=>'application/json'
                                ];
                                $body=[
                                    'repository'=>$request->repository,
                                    'provider'=>$request->repository_provider,
                                    'branch'=>$request->branch
                                ];
                                $res = $client->request('POST','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$dataTypeContent->site_id.'/git', [
                                    'form_params' => $body, 
                                    'headers' => $headers 
                                ]);
                                //print_r($res);exit;
                                //echo $res->getStatusCode();exit;
                                if ($res->getStatusCode() == 200) { // 200 OK
                                        //$response_data = $res->getBody()->getContents();
                                        //$response_data=json_decode($response_data);
                                        //print_r($res->getBody());exit;
                                    //$this->insertUpdateData($request, $slug, $dataType->editRows, $data);
                                    DB::table('user_sites')->where('id', $dataTypeContent->id)->update(['repository' => $request->repository,'repository_provider'=>$request->repository_provider,'repository_branch'=>$request->branch]);
                                    event(new BreadDataUpdated($dataType, $data));
                                    return response()->json(['message' => "Deployment saved success and please wait untill it finished","status"=>true]);exit;
                                }
                            }catch(ClientException $e){
                                return response()->json(['message' => "Client exception : ".$responseBodyAsString,"status"=>false]);exit;
                            }catch(RequestException $e){
                                return response()->json(['message' => "Request exception","status"=>false]);exit;
                            }catch(BadResponseException $e){
                                return response()->json(['message' => "Bad response exception","status"=>false]);exit;
                            }catch(ServerException $e){
                                return response()->json(['message' => "Server exception","status"=>false]);exit;
                            }catch(ConnectException $e){
                                return response()->json(['message' => "Connect exception","status"=>false]);exit;
                            }catch( TooManyRedirectsException $e){
                                return response()->json(['message' => "TooMany redirects exception","status"=>false]);exit;
                            }catch(Exception $e){
                                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
                            }catch(\Exception $e){
                                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
                            }
                        }
                    }else{
                        return response()->json(['message' => "Site was not found on server.","status"=>false]);exit;
                    }
                    $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
                        //var_dump($data);exit;
                    event(new BreadDataAdded($dataType, $data));
                    return response()->json(['message' => "Deployment saved success and please wait untill it finished","status"=>true]);exit;
                }else{
                    return response()->json(['message' => "Can not fetch detail from server.","status"=>false]);exit;
                }
            }catch(ClientException $e){
                return response()->json(['message' => "Client exception : ".$responseBodyAsString,"status"=>false]);exit;
            }catch(RequestException $e){
                return response()->json(['message' => "Request exception","status"=>false]);exit;
            }catch(BadResponseException $e){
                return response()->json(['message' => "Bad response exception","status"=>false]);exit;
            }catch(ServerException $e){
                return response()->json(['message' => "Server exception","status"=>false]);exit;
            }catch(ConnectException $e){
                return response()->json(['message' => "Connect exception","status"=>false]);exit;
            }catch( TooManyRedirectsException $e){
                return response()->json(['message' => "TooMany redirects exception","status"=>false]);exit;
            }catch(Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }catch(\Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }
            return response()->json(['message' => "Site is not installed","status"=>false]);exit;
        }else{
            try{
                return response()->json(['message' => "installed successfully","status"=>true]);exit;
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                    //'Accept'=>'application/json',
                    //'Content-Type'=>'application/json'
                ];
                $body=[
                    'repository'=>$request->repository,
                    'provider'=>$request->repository_provider,
                    'branch'=>$request->branch
                ];
                $res = $client->request('POST','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$dataTypeContent->site_id.'/git', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);

                if ($res->getStatusCode() == 200) { // 200 OK
                    //$this->insertUpdateData($request, $slug, $dataType->editRows, $data);
                    DB::table('user_sites')->where('id', $dataTypeContent->id)->update(['repository' => $request->repository,'repository_provider'=>$request->repository_provider,'repository_branch'=>$request->branch]);
                    event(new BreadDataUpdated($dataType, $data));
                    return response()->json(['message' => "Deployment saved success and please wait untill it finished","status"=>true]);exit;
                }
            }catch(ClientException $e){
                return response()->json(['message' => "Client exception : ".$responseBodyAsString,"status"=>false]);exit;
            }catch(RequestException $e){
                return response()->json(['message' => "Request exception","status"=>false]);exit;
            }catch(BadResponseException $e){
                return response()->json(['message' => "Bad response exception","status"=>false]);exit;
            }catch(ServerException $e){
                return response()->json(['message' => "Server exception","status"=>false]);exit;
            }catch(ConnectException $e){
                return response()->json(['message' => "Connect exception","status"=>false]);exit;
            }catch( TooManyRedirectsException $e){
                return response()->json(['message' => "TooMany redirects exception","status"=>false]);exit;
            }catch(Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }catch(\Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }
        }
    }
    public function getDeployScriptAjax(Request $request)
    {
        if(!$request->has("id")){
            return response()->json(['message' => "Site was not found",'status'=>false]);exit;
        }
        $id=$request->get("id");
        $customAttributes=array();
        $viewType="deployScript";

        $slug = 'user-sites';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
            //Checking if the user belongs to the data
        if(!DB::table($dataType->name)->where('id', $id)->where('user_id',app('VoyagerAuth')->user()->id)->exists()){
            return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
        }
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        /*Get script from server*/
        if(empty($dataTypeContent->deployment_script)){

            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
            $server = DB::table("user_apps")->where('id', $dataTypeContent->server_id)->where('user_id',app('VoyagerAuth')->user()->id)->first();
            if(!isset($server->id) || empty($server->server_id)){
                return response()->json(['message' => "Server was not found","status"=>false]);exit;
            }
            if($dataTypeContent->site_status != "installed"){
                return response()->json(['message' => "Site is not installed","status"=>false]);exit;
            }
            try{
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                    //'Accept'=>'application/json',
                    //'Content-Type'=>'application/json'
                ];
                $body=[];
                $res = $client->request('GET','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$dataTypeContent->site_id.'/deployment/script', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);
                if ($res->getStatusCode() == 200) { // 200 OK
                    $response_data = $res->getBody()->getContents();
                    if(!empty($response_data)){
                        $dataTypeContent->deployment_script=trim($response_data);
                        return response()->json(['message' => "get deploy script successfully","status"=>true,"data"=>$dataTypeContent->deployment_script]);exit;
                    }
                }else{
                    return response()->json(['message' => "Failed to get default script from server","status"=>false]);exit;
                }
                
            }catch(ClientException $e){
                return response()->json(['message' => "Client exception : ".$responseBodyAsString,"status"=>false]);exit;
            }catch(RequestException $e){
                return response()->json(['message' => "Request exception","status"=>false]);exit;
            }catch(BadResponseException $e){
                return response()->json(['message' => "Bad response exception","status"=>false]);exit;
            }catch(ServerException $e){
                return response()->json(['message' => "Server exception","status"=>false]);exit;
            }catch(ConnectException $e){
                return response()->json(['message' => "Connect exception","status"=>false]);exit;
            }catch( TooManyRedirectsException $e){
                return response()->json(['message' => "TooMany redirects exception","status"=>false]);exit;
            }catch(Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }catch(\Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }
        }else{
            return response()->json(['message' => "get deploy script successfully","status"=>true,"data"=>$dataTypeContent->deployment_script]);exit;
        }
    }
    public function deployScriptAjax(Request $request)
    {
        if(!$request->has("id")){
            return response()->json(['message' => "Site was not found",'status'=>false]);exit;
        }
        $id=$request->get("id");
        $slug = 'user-sites';
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
            //Checking if the user belongs to the data
        if(!DB::table($dataType->name)->where('id', $id)->where('user_id',app('VoyagerAuth')->user()->id)->exists()){
            return response()->json(['message' => "Site was not found",'status'=>false]);exit;
        }

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        }

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $rules=array();
        $messages=array();
        $customAttributes=array();
        
        $rules["deployment_script"]="required";
        $messages["deployment_script.required"]="Please enter deploy script";

        $validate = Validator::make($request->all(), $rules, $messages, $customAttributes);
        if($validate->fails()){
            foreach ($validate->errors()->all() as $error) {
                return response()->json(['message' => $error,'status'=>false]);exit;
            }
        }
        $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        $server = DB::table("user_apps")->where('id', $dataTypeContent->server_id)->where('user_id',app('VoyagerAuth')->user()->id)->first();
        if(!isset($server->id) || empty($server->server_id)){
            return response()->json(['message' => "Server was not found",'status'=>false]);exit;
        }
        if($dataTypeContent->site_status != "installed"){
            return response()->json(['message' => "Site is not installed",'status'=>false]);exit;
        }else{
            try{
                return response()->json(['message' => "done",'status'=>true]);exit;  
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                    //'Accept'=>'application/json',
                    //'Content-Type'=>'application/json'
                ];
                $body=[
                    'content'=>$request->deployment_script,
                ];
                $res = $client->request('PUT','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$dataTypeContent->site_id.'/deployment/script', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);
                if ($res->getStatusCode() == 200) { // 200 OK
                    DB::table('user_sites')->where('id', $dataTypeContent->id)->update(['deployment_script' => $request->deployment_script]);
                    event(new BreadDataUpdated($dataType, $data));
                    return response()->json(['message' => "Repo install successfully","status"=>true]);exit;
                }else{
                    return response()->json(['message' => "Can not update script to server","status"=>false]);exit;
                }
                
            }catch(ClientException $e){
                $responseBodyAsString = $e->getResponse()->getBody()->getContents();
                return response()->json(['message' => "Client exception : ".$responseBodyAsString,"status"=>false]);exit;
            }catch(RequestException $e){
                return response()->json(['message' => "Request exception","status"=>false]);exit;
            }catch(BadResponseException $e){
                return response()->json(['message' => "Bad response exception","status"=>false]);exit;
            }catch(ServerException $e){
                return response()->json(['message' => "Server exception","status"=>false]);exit;
            }catch(ConnectException $e){
                return response()->json(['message' => "Connect exception","status"=>false]);exit;
            }catch( TooManyRedirectsException $e){
                return response()->json(['message' => "TooMany redirects exception","status"=>false]);exit;
            }catch(Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }catch(\Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }
        }
    }
    public function getEnvAjax(Request $request)
    {
        if(!$request->has("id")){
            return response()->json(['message' => "Site was not found",'status'=>false]);exit;
        }
        $id=$request->get("id");
        $customAttributes=array();
        $viewType="env";

        $slug = 'user-sites';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
            //Checking if the user belongs to the data
        if(!DB::table($dataType->name)->where('id', $id)->where('user_id',app('VoyagerAuth')->user()->id)->exists()){
            return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
        }
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        /*Get script from server*/
        if(empty($dataTypeContent->env_file)){

            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
            $server = DB::table("user_apps")->where('id', $dataTypeContent->server_id)->where('user_id',app('VoyagerAuth')->user()->id)->first();
            if(!isset($server->id) || empty($server->server_id)){
                return response()->json(['message' => "Server was not found","status"=>false]);exit;
            }
            if($dataTypeContent->site_status != "installed"){
                return response()->json(['message' => "Site is not installed","status"=>false]);exit;
            }
            try{
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                    //'Accept'=>'application/json',
                    //'Content-Type'=>'application/json'
                ];
                $body=[];
                $res = $client->request('GET','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$dataTypeContent->site_id.'/env', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);
                if ($res->getStatusCode() == 200) { // 200 OK
                    $response_data = $res->getBody()->getContents();
                    if(!empty($response_data)){
                        $dataTypeContent->env_file=trim($response_data);
                        return response()->json(['message' => "Get default env successfully","status"=>true,"data"=>$dataTypeContent->env_file]);exit;
                    }
                }
                return response()->json(['message' => "Failed to get default env","status"=>false]);exit;
                
            }catch(ClientException $e){
                $responseBodyAsString = $e->getResponse()->getBody()->getContents();
                return response()->json(['message' => "Client exception : ".$responseBodyAsString,"status"=>false]);exit;
            }catch(RequestException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Request exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
                return response()->json(['message' => "Request exception","status"=>false]);exit;
            }catch(BadResponseException $e){
                return response()->json(['message' => "Bad response exception","status"=>false]);exit;
            }catch(ServerException $e){
                return response()->json(['message' => "Server exception","status"=>false]);exit;
            }catch(ConnectException $e){
                return response()->json(['message' => "Connect exception","status"=>false]);exit;
            }catch( TooManyRedirectsException $e){
                return response()->json(['message' => "TooMany redirects exception","status"=>false]);exit;
            }catch(Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }catch(\Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }
        }
        return response()->json(['message' => "Get default env successfully","status"=>true,"data"=>$dataTypeContent->env_file]);exit;
    }

    public function updateEnvAjax(Request $request)
    {
        if(!$request->has("id")){
            return response()->json(['message' => "Site was not found",'status'=>false]);exit;
        }
        $id=$request->get("id");
        $slug = 'user-sites';
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
            //Checking if the user belongs to the data
        if(!DB::table($dataType->name)->where('id', $id)->where('user_id',app('VoyagerAuth')->user()->id)->exists()){
            return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
        }

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        }

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $rules=array();
        $messages=array();
        $customAttributes=array();
        
        $rules["env_file"]="required";
        $messages["env_file.required"]="Please enter env script";

        $validate = Validator::make($request->all(), $rules, $messages, $customAttributes);
        if($validate->fails()){
            foreach ($validate->errors()->all() as $error) {
                return response()->json(['message' => $error,'status'=>false]);exit;
            }
        }
        $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        $server = DB::table("user_apps")->where('id', $dataTypeContent->server_id)->where('user_id',app('VoyagerAuth')->user()->id)->first();
        if(!isset($server->id) || empty($server->server_id)){
            return response()->json(['message' => "Server was not found",'status'=>false]);exit;
        }
        if($dataTypeContent->site_status != "installed"){
            return response()->json(['message' => "Site is not installed",'status'=>false]);exit;
        }else{
            try{
                //remove
                //return response()->json(['message' => "Env file updated successfully.",'status'=>true]);exit;
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                    //'Accept'=>'application/json',
                    //'Content-Type'=>'application/json'
                ];
                $body=[
                    'content'=>$request->env_file,
                ];
                $res = $client->request('PUT','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$dataTypeContent->site_id.'/env', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);
                if ($res->getStatusCode() == 200) { // 200 OK
                    DB::table('user_sites')->where('id', $dataTypeContent->id)->update(['env_file' => $request->env_file]);
                    event(new BreadDataUpdated($dataType, $data));
                    return response()->json(['message' => "Env file updated successfully.",'status'=>true]);exit;
                }else{
                    return response()->json(['message' => "Can not update env file to server",'status'=>false]);exit;
                }
                
            }catch(ClientException $e){
                return response()->json(['message' => "Client exception : ".$responseBodyAsString,'status'=>false]);exit;
            }catch(RequestException $e){
                return response()->json(['message' => "Request exception",'status'=>false]);exit;
            }catch(BadResponseException $e){
                return response()->json(['message' => "Bad response exception",'status'=>false]);exit;
            }catch(ServerException $e){
                return response()->json(['message' => "Server exception",'status'=>false]);exit;
            }catch(ConnectException $e){
                return response()->json(['message' => "Connect exception",'status'=>false]);exit;
            }catch( TooManyRedirectsException $e){
                return response()->json(['message' => "TooMany redirects exception",'status'=>false]);exit;
            }catch(Exception $e){
                return response()->json(['message' => "Something wemt to wrong",'status'=>false]);exit;
            }catch(\Exception $e){
                return response()->json(['message' => "Something wemt to wrong",'status'=>false]);exit;
            }
        }
    }
}