<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class ActionDeployNow extends AbstractAction
{
    public function getTitle()
    {
        return 'Deploy Now';
    }

    public function getIcon()
    {
        return 'voyager-eye';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-success pull-right',
        ];
    }

    public function getDefaultRoute()
    {
        return route('VoyegarUserSiteDeployNow',$this->data->id);
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'user-sites';
    }
}