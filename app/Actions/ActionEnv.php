<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class ActionEnv extends AbstractAction
{
    public function getTitle()
    {
        return 'Env File';
    }

    public function getIcon()
    {
        return 'voyager-eye';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-warning pull-right',
        ];
    }

    public function getDefaultRoute()
    {
        return route('VoyegarUserSiteEnvUpdate',$this->data->id);
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'user-sites';
    }
}