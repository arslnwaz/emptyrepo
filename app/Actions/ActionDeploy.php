<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class ActionDeploy extends AbstractAction
{
    public function getTitle()
    {
        return 'Deploy';
    }

    public function getIcon()
    {
        return 'voyager-eye';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-dark pull-right',
        ];
    }

    public function getDefaultRoute()
    {
        return route('VoyegarUserSiteDeploy',$this->data->id);
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'user-sites';
    }
}