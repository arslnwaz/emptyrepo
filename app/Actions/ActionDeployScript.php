<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class ActionDeployScript extends AbstractAction
{
    public function getTitle()
    {
        return 'Deploy Script';
    }

    public function getIcon()
    {
        return 'voyager-eye';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-info pull-right',
        ];
    }

    public function getDefaultRoute()
    {
        return route('VoyegarUserSiteDeployScript',$this->data->id);
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'user-sites';
    }
}