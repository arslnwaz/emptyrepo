@extends('theme::layouts.app')


@section('content')

<div class="uk-container">


	<div class="uk-child-width-1-1@m uk-grid-match uk-margin-top uk-grid">
		<div>

			<div class="uk-card uk-card-large">
				<div class="uk-card-badge">Email Only</div>
				<div class="uk-card-header">
					<div class="uk-grid-small uk-flex-middle" uk-grid>
						<div class="uk-width-auto">
							<div uk-icon="icon: lifesaver; ratio: 2.8" class="welcome-icon"></div>
						</div>
						<div class="uk-width-expand">
							<h3 class="uk-card-title uk-margin-remove-bottom uk-blue">Get Support From Our Friendly Team</h3>
							<p class="uk-text-meta uk-margin-remove-top">Enter Your Query Below Or Use The Chat Icon At The Very Bottom Right</p>
						</div>
					</div>
				</div>
				<div class="uk-card-body ">
					<p>Please enter a detailed description of your support inquiry below and we will get back to you via email as soon as we can.</p>
					<form class="flex uk-form-horizontal" action="{{ route('support.send') }}" method="POST">
						@csrf
						<input class="uk-input required email" type="email" name="email" placeholder="Your Email Address">
						<input class="uk-input" type="text" name="subject" placeholder="Subject" value="{{ old('subject') }}">
						<textarea class=".uk-form-help-block" name="message" cols="30" rows="10" placeholder="Your Support Message" >
							{{ old('message') }}
						</textarea>
						<button class="uk-margin uk-text-left@m uk-text-center uk-scrollspy-inview uk-animation-slide-left-medium el-content uk-button uk-button-primary"> Send Message </button>
					</form>
				</div>
				<div class="uk-card-footer">
					<script type="text/javascript">
						!function(e,t,n){function a(){var e=t.getElementsByTagName("script")[0],n=t.createElement("script");n.type="text/javascript",n.async=!0,n.src="https://beacon-v2.helpscout.net",e.parentNode.insertBefore(n,e)}if(e.Beacon=n=function(t,n,a){e.Beacon.readyQueue.push({method:t,options:n,data:a})},n.readyQueue=[],"complete"===t.readyState)return a();e.attachEvent?e.attachEvent("onload",a):e.addEventListener("load",a,!1)}(window,document,window.Beacon||function(){});
					</script>
					<script type="text/javascript">window.Beacon('init', 'c8db6fc4-45ef-4286-bd69-90ec2c988747')</script>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection