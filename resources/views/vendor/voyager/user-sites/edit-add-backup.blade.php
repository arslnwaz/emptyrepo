@extends('voyager::master')

@section('page_title', __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
<h1 class="page-title">
    <i class="{{ $dataType->icon }}"></i>
    {{ __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
</h1>
@stop

@section('content')

@if(isset($viewType) && $viewType == "deploy" )
<!-- deploy script-->
<div class="page-content container-fluid">
    <form class="form-edit-add" role="form"
    action="{{route('VoyegarUserSiteDeploy', $dataTypeContent->id)}}" method="POST" enctype="multipart/form-data" autocomplete="off">
    <!-- PUT Method if we are editing -->
    @if(isset($dataTypeContent->id))
    {{ method_field("PUT") }}
    @endif
    {{ csrf_field() }}

    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-bordered">
                {{-- <div class="panel"> --}}
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="domain">Repository</label>
                            <input type="repository" class="form-control" id="repository" name="repository" placeholder="Enter repository"
                            value="@if(isset($dataTypeContent->repository)){{ $dataTypeContent->repository }}@endif">
                        </div>

                        <div class="form-group">
                            <label for="name">Repository Provider</label>
                            <select name="repository_provider" id="repository_provider" class="select2" placeholder="Select provider">
                                <option value=""></option>
                                <option value="bitbucket" <?php echo (isset($dataTypeContent->repository_provider) && $dataTypeContent->repository_provider == "bitbucket")? "selected":""?>>bitbucket</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="branch">Branch</label>
                            <input type="branch" class="form-control" id="branch" name="branch" placeholder="Enter branch"
                            value="@if(isset($dataTypeContent->repository_branch)){{ $dataTypeContent->repository_branch }}@endif">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-primary pull-right save">
            {{ __('voyager::generic.save') }}
        </button>
    </form>
    <iframe id="form_target" name="form_target" style="display:none"></iframe>
</div>
@elseif(isset($viewType) && $viewType == "deployScript" )
<!-- deploy script-->
<div class="page-content container-fluid">
    <form class="form-edit-add" role="form"
    action="{{route('VoyegarUserSiteDeployScript', $dataTypeContent->id)}}" method="POST" enctype="multipart/form-data" autocomplete="off">
    <!-- PUT Method if we are editing -->
    @if(isset($dataTypeContent->id))
    {{ method_field("PUT") }}
    @endif
    {{ csrf_field() }}

    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-bordered">
                {{-- <div class="panel"> --}}
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="domain">Deplo Script</label>
                            <textarea  class="form-control" id="deployment_script" name="deployment_script" rows="10">
                                <?php echo $dataTypeContent->deployment_script; ?>
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-primary pull-right save">
            {{ __('voyager::generic.save') }}
        </button>
    </form>
    <iframe id="form_target" name="form_target" style="display:none"></iframe>
</div>
@elseif(isset($viewType) && $viewType == "env" )
<!-- deploy script-->
<div class="page-content container-fluid">
    <form class="form-edit-add" role="form"
    action="{{route('VoyegarUserSiteEnvUpdate', $dataTypeContent->id)}}" method="POST" enctype="multipart/form-data" autocomplete="off">
    <!-- PUT Method if we are editing -->
    @if(isset($dataTypeContent->id))
    {{ method_field("PUT") }}
    @endif
    {{ csrf_field() }}

    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-bordered">
                {{-- <div class="panel"> --}}
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="domain">Env Script</label>
                            <textarea  class="form-control" id="env_file" name="env_file" rows="10">
                                <?php echo $dataTypeContent->env_file; ?>
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-primary pull-right save">
            {{ __('voyager::generic.save') }}
        </button>
    </form>
    <iframe id="form_target" name="form_target" style="display:none"></iframe>
</div>
@else
<!-- create site-->
<div class="page-content container-fluid">
    <form class="form-edit-add" role="form"
    action="{{ (isset($dataTypeContent->id)) ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->id) : route('voyager.'.$dataType->slug.'.store') }}"
    method="POST" enctype="multipart/form-data" autocomplete="off">
    <!-- PUT Method if we are editing -->
    @if(isset($dataTypeContent->id))
    {{ method_field("PUT") }}
    @endif
    {{ csrf_field() }}

    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-bordered">
                {{-- <div class="panel"> --}}
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="role_id">Select Server</label>
                            @php $servers = App\UserApp::all(); @endphp
                            <select name="server_id" id="server_id" class="select2" placeholder="Select server">
                                @foreach($servers as $server)
                                <option value="{{ $server->id }}">{{ $server->app_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="domain">Domain</label>
                            <input type="domain" class="form-control" id="domain" name="domain" placeholder="Enter domain name"
                            value="@if(isset($dataTypeContent->domain)){{ $dataTypeContent->domain }}@else{{'test1.rapidstartup.io'}}@endif">
                        </div>

                        <div class="form-group">
                            <label for="name">Project Type</label>
                            <select name="project_type" id="project_type" class="select2" placeholder="Select project type">
                                <option value="php">PHP</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="username" class="form-control" id="username" name="username" placeholder="Username"
                            value="@if(isset($dataTypeContent->username)){{ $dataTypeContent->username }}@else{{'test1'}}@endif">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-primary pull-right save">
            {{ __('voyager::generic.save') }}
        </button>
    </form>
    <iframe id="form_target" name="form_target" style="display:none"></iframe>
</div>
@endif

@stop

@section('javascript')
<script>
    $('document').ready(function () {
        $('.toggleswitch').bootstrapToggle();
    });
</script>
@stop
