@extends('voyager::master')

@section('page_title', __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<style type="text/css">
    /* Latest compiled and minified CSS included as External Resource*/

    /* Optional theme */

    /*@import url('//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css');*/
    body {
        margin-top:30px;
    }
    .stepwizard-step p {
        margin-top: 0px;
        color:#000;
    }
    .stepwizard-row {
        display: table-row;
    }
    .stepwizard {
        display: table;
        width: 100%;
        position: relative;
    }
    .stepwizard-step button[disabled] {
    /*opacity: 1 !important;
    filter: alpha(opacity=100) !important;*/
}
.stepwizard .btn.disabled, .stepwizard .btn[disabled], .stepwizard fieldset[disabled] .btn {
    opacity:1 !important;
    color:#bbb;
}
.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content:" ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-index: 0;
}
.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
.btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}
.voyager .btn.btn-success{
    background: #DA4B54;
}
.voyager .panel.panel-primary .panel-heading{
    background:#353d47;
    padding: 0px;
}
.panel-body {
    margin: 15px 15px;
}
</style>
@stop

@section('page_header')
<h1 class="page-title">
    <i class="{{ $dataType->icon }}"></i>
    {{ __('voyager::generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
</h1>
@stop

@section('content')

@if(isset($viewType) && $viewType == "deploy" )
<!-- deploy script-->
<div class="page-content container-fluid">
    <form class="form-edit-add" role="form"
    action="{{route('VoyegarUserSiteDeploy', $dataTypeContent->id)}}" method="POST" enctype="multipart/form-data" autocomplete="off">
    <!-- PUT Method if we are editing -->
    @if(isset($dataTypeContent->id))
    {{ method_field("PUT") }}
    @endif
    {{ csrf_field() }}

    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-bordered">
                {{-- <div class="panel"> --}}
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="domain">Repository</label>
                            <input type="repository" class="form-control" id="repository" name="repository" placeholder="Enter repository"
                            value="@if(isset($dataTypeContent->repository)){{ $dataTypeContent->repository }}@endif">
                        </div>

                        <div class="form-group">
                            <label for="name">Repository Provider</label>
                            <select name="repository_provider" id="repository_provider" class="select2" placeholder="Select provider">
                                <option value=""></option>
                                <option value="bitbucket" <?php echo (isset($dataTypeContent->repository_provider) && $dataTypeContent->repository_provider == "bitbucket")? "selected":""?>>bitbucket</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="branch">Branch</label>
                            <input type="branch" class="form-control" id="branch" name="branch" placeholder="Enter branch"
                            value="@if(isset($dataTypeContent->repository_branch)){{ $dataTypeContent->repository_branch }}@endif">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-primary pull-right save">
            {{ __('voyager::generic.save') }}
        </button>
    </form>
    <iframe id="form_target" name="form_target" style="display:none"></iframe>
</div>
@elseif(isset($viewType) && $viewType == "deployScript" )
<!-- deploy script-->
<div class="page-content container-fluid">
    <form class="form-edit-add" role="form"
    action="{{route('VoyegarUserSiteDeployScript', $dataTypeContent->id)}}" method="POST" enctype="multipart/form-data" autocomplete="off">
    <!-- PUT Method if we are editing -->
    @if(isset($dataTypeContent->id))
    {{ method_field("PUT") }}
    @endif
    {{ csrf_field() }}

    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-bordered">
                {{-- <div class="panel"> --}}
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="domain">Deplo Script</label>
                            <textarea  class="form-control" id="deployment_script" name="deployment_script" rows="10">
                                <?php echo $dataTypeContent->deployment_script; ?>
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-primary pull-right save">
            {{ __('voyager::generic.save') }}
        </button>
    </form>
    <iframe id="form_target" name="form_target" style="display:none"></iframe>
</div>
@elseif(isset($viewType) && $viewType == "env" )
<!-- deploy script-->
<div class="page-content container-fluid">
    <form class="form-edit-add" role="form"
    action="{{route('VoyegarUserSiteEnvUpdate', $dataTypeContent->id)}}" method="POST" enctype="multipart/form-data" autocomplete="off">
    <!-- PUT Method if we are editing -->
    @if(isset($dataTypeContent->id))
    {{ method_field("PUT") }}
    @endif
    {{ csrf_field() }}

    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-bordered">
                {{-- <div class="panel"> --}}
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="domain">Env Script</label>
                            <textarea  class="form-control" id="env_file" name="env_file" rows="10">
                                <?php echo $dataTypeContent->env_file; ?>
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-primary pull-right save">
            {{ __('voyager::generic.save') }}
        </button>
    </form>
    <iframe id="form_target" name="form_target" style="display:none"></iframe>
</div>
@else
<!-- create site-->
<div class="page-content container-fluid">
    <form class="form-edit-add" role="form"
    action="{{ (isset($dataTypeContent->id)) ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->id) : route('voyager.'.$dataType->slug.'.store') }}"
    method="POST" enctype="multipart/form-data" autocomplete="off">
    <!-- PUT Method if we are editing -->
    @if(isset($dataTypeContent->id))
    {{ method_field("PUT") }}
    @endif
    {{ csrf_field() }}
    <?php
    $step=1;
    $isCreated=false;
    $isInstallRepo=false;
    if(isset($dataTypeContent->id))
    {
        $isCreated=true;
        if(isset($dataTypeContent->repository) && !empty($dataTypeContent->repository))
        {
            $isInstallRepo=true;
        }
    }
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </div>
        </div>
    </div>
    <!-- Steps -->
    <div class="container">
        <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step col-xs-3"> 
                    <a href="#step-1" type="button" class="btn btn-success btn-circle">1</a>
                    <p><small>Create Site</small></p>
                </div>
                <div class="stepwizard-step col-xs-3"> 
                    <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                    <p><small>Install Repo</small></p>
                </div>
                <div class="stepwizard-step col-xs-3"> 
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                    <p><small>Edit Deploy Script</small></p>
                </div>
                <div class="stepwizard-step col-xs-3"> 
                    <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                    <p><small>Setting Env</small></p>
                </div>
            </div>
        </div>
        <form class="form-edit-add" role="form"
        action="#" method="POST" enctype="multipart/form-data" autocomplete="off">
        <input type="hidden" name="id" id="id" value="{{ isset($dataTypeContent->id)?$dataTypeContent->id:'' }}"/>
        <div class="panel panel-primary setup-content" id="step-1">
            <div class="panel-heading">
                <h3 class="panel-title">Create Site</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="role_id">Select Server</label>
                    @php $servers = App\UserApp::all(); @endphp
                    <select name="server_id" id="server_id" class="select2" placeholder="Select server" class="form-control">
                        @foreach($servers as $server)
                        <option value="{{ $server->id }}" @if(isset($dataTypeContent->server_id) && $dataTypeContent->server_id == $server->id){{ "selected" }}@else{{ "" }}@endif>{{ $server->app_name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="domain">Domain</label>
                    <input type="domain" class="form-control" id="domain" name="domain" placeholder="Enter domain name"
                    value="@if(isset($dataTypeContent->domain)){{ $dataTypeContent->domain }}@else{{'test1.rapidstartup.io'}}@endif">
                </div>

                <div class="form-group">
                    <label for="name">Project Type</label>
                    <select name="project_type" id="project_type" class="select2" placeholder="Select project type" class="form-control">
                        <option value="php" @if(isset($dataTypeContent->project_type) && $dataTypeContent->project_type == "php"){{ "selected" }}@else{{ "" }}@endif>PHP</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="username" class="form-control" id="username" name="username" placeholder="Username"
                    value="@if(isset($dataTypeContent->username)){{ $dataTypeContent->username }}@else{{'test1'}}@endif">
                </div>
                <button class="btn btn-primary nextBtn pull-right" id="step-1_button" type="button">Next</button>
            </div>
        </div>

        <div class="panel panel-primary setup-content" id="step-2">
            <div class="panel-heading">
                <h3 class="panel-title">Install Repository</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="domain">Repository</label>
                    <input type="repository" class="form-control" id="repository" name="repository" placeholder="Enter repository"
                    value="@if(isset($dataTypeContent->repository)){{ $dataTypeContent->repository }}@endif">
                </div>

                <div class="form-group">
                    <label for="name">Repository Provider</label>
                    <select name="repository_provider" id="repository_provider" class="select2" placeholder="Select provider" class="form-control">
                        <option value=""></option>
                        <option value="bitbucket" <?php echo (isset($dataTypeContent->repository_provider) && $dataTypeContent->repository_provider == "bitbucket")? "selected":""?>>bitbucket</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="branch">Branch</label>
                    <input type="branch" class="form-control" id="branch" name="branch" placeholder="Enter branch"
                    value="@if(isset($dataTypeContent->repository_branch)){{ $dataTypeContent->repository_branch }}@endif">
                </div>
                <button class="btn btn-primary nextBtn pull-right" id="step-2_button" type="button">Next</button>
            </div>
        </div>

        <div class="panel panel-primary setup-content" id="step-3">
            <div class="panel-heading">
                <h3 class="panel-title">Depoly Script</h3>
            </div>
            <div class="panel-body">
                <div class="panel-body">
                    <div class="form-group">
                        <label for="domain">Deplo Script</label>
                        <textarea  class="form-control" id="deployment_script" name="deployment_script" rows="10">
                            <?php echo (isset($dataTypeContent->deployment_script))?$dataTypeContent->deployment_script:""; ?>
                        </textarea>
                    </div>
                </div>
                <button class="btn btn-primary nextBtn pull-right" id="step-3_button" type="button">Next</button>
            </div>
        </div>

        <div class="panel panel-primary setup-content" id="step-4">
            <div class="panel-heading">
                <h3 class="panel-title">Update Env File</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="domain">Env Script</label>
                    <textarea  class="form-control" id="env_file" name="env_file" rows="10">
                        <?php echo (isset($dataTypeContent->env_file))?$dataTypeContent->env_file:""; ?>
                    </textarea>
                </div>
                <button class="btn btn-success nextBtn pull-right" id="step-4_button" type="button">Finish!</button>
                <a href="{{route('voyager.user-sites.index')}}" class="hide" id="finalredirect"></a>
            </div>
        </div>
    </form>
    <iframe id="form_target" name="form_target" style="display:none"></iframe>
</div>

@endif



@stop

@section('javascript')
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
<script>
    $('document').ready(function () {
        $('.toggleswitch').bootstrapToggle();
    });
    $(document).ready(function () {

        var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');

        allWells.hide();

        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
            $item = $(this);

            if (!$item.hasClass('disabled')) {
                //navListItems.removeClass('btn-success').addClass('btn-default');
                $item.addClass('btn-success');
                allWells.hide();
                $target.show();
                $target.find('input:eq(0)').focus();
            }
        });

        allNextBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;
            var finalredirect=$("#finalredirect");
            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }

            if (isValid)
            {
                var data1 = new FormData();    
                if(curStepBtn == "step-1"){
                    console.log("1");
                    data1.append('server_id',$("#server_id").val());
                    data1.append('domain', $("#domain").val());
                    data1.append('project_type', $("#project_type").val());
                    data1.append('username', $("#username").val());
                    $.ajax({
                        type: "POST",
                        url: "{{route('VoyegarUserSiteCreateAjax')}}",
                        data: data1,
                        processData: false,
                        contentType: false,
                        success: function(response){

                            if(response.hasOwnProperty('status') && response.status == true){
                                //console.log("success");
                                $("#id").val(response.id);
                                nextStepWizard.removeAttr('disabled').trigger('click');
                            }else if(response.hasOwnProperty('message')){
                                alert(response.message);
                            }else{
                                alert("Something went to wrong. Please try again later");
                            }
                            //nextStepWizard.removeAttr('disabled').trigger('click');
                        },
                        error: function(response){
                            alert("Something went to wrong. Please try again later");
                        }
                    });
                }else if(curStepBtn == "step-2"){
                    data1.append('id',$("#id").val());
                    data1.append('repository', $("#repository").val());
                    data1.append('repository_provider', $("#repository_provider").val());
                    data1.append('branch', $("#branch").val());
                    $.ajax({
                        type: "POST",
                        url: "{{route('VoyegarUserSiteInstallRepoAjax')}}",
                        data: data1,
                        processData: false,
                        contentType: false,
                        success: function(response){

                            if(response.hasOwnProperty('status') && response.status == true){
                                //get default script
                                $.ajax({
                                    type: "POST",
                                    url: "{{route('VoyegarUserSiteGetDeployScriptAjax')}}",
                                    data: data1,
                                    processData: false,
                                    contentType: false,
                                    success: function(response){
                                        if(response.hasOwnProperty('status') && response.status == true){
                                            $("#deployment_script").val(response.data);
                                            nextStepWizard.removeAttr('disabled').trigger('click');
                                        }else if(response.hasOwnProperty('message')){
                                            alert(response.message);
                                        }else{
                                            alert("Something went to wrong. Please try again later");
                                        }
                                    },
                                    error: function(response){
                                        alert("Something went to wrong. Please try again later");
                                    }
                                });
                            }else if(response.hasOwnProperty('message')){
                                alert(response.message);
                            }else{
                                alert("Something went to wrong. Please try again later");
                            }
                        },
                        error: function(response){
                            alert("Something went to wrong. Please try again later");
                        }
                    });
                }else if(curStepBtn == "step-3"){
                    //console.log("3");
                    data1.append('id',$("#id").val());
                    data1.append('deployment_script', $("#VoyegarUserSiteDeployScriptAjax").val());
                    $.ajax({
                        type: "POST",
                        url: "{{route('VoyegarUserSiteGetEnvAjax')}}",
                        data: data1,
                        processData: false,
                        contentType: false,
                        success: function(response){
                            if(response.hasOwnProperty('status') && response.status == true){
                                alert(response.message);
                                $.ajax({
                                    type: "POST",
                                    url: "{{route('VoyegarUserSiteGetEnvAjax')}}",
                                    data: data1,
                                    processData: false,
                                    contentType: false,
                                    success: function(response){
                                        if(response.hasOwnProperty('status') && response.status == true){
                                            $("#env_file").val(response.data);
                                            nextStepWizard.removeAttr('disabled').trigger('click');
                                        }else if(response.hasOwnProperty('message')){
                                            alert(response.message);
                                        }else{
                                            alert("Something went to wrong. Please try again later");
                                        }
                                    },
                                    error: function(response){
                                        alert("Something went to wrong. Please try again later");
                                    }
                                });
                            }else if(response.hasOwnProperty('message')){
                                alert(response.message);
                            }else{
                                alert("Something went to wrong. Please try again later");
                            }
                        },
                        error: function(response){
                            alert("Something went to wrong. Please try again later");
                        }
                    });                    
                }else if(curStepBtn == "step-4"){
                    //console.log("4");
                    data1.append('id',$("#id").val());
                    data1.append('env_file', $("#env_file").val());
                    $.ajax({
                        type: "POST",
                        url: "{{route('VoyegarUserSiteUpdateEnvAjax')}}",
                        data: data1,
                        processData: false,
                        contentType: false,
                        success: function(response){
                            if(response.hasOwnProperty('status') && response.status == true){
                                //alert(response.message);
                                //finalredirect.trigger('click');
                                location.href = finalredirect.attr("href");
                                //console.log(finalredirect.attr("href"));
                            }else if(response.hasOwnProperty('message')){
                                alert(response.message);
                            }else{
                                alert("Something went to wrong. Please try again later");
                            }
                        },
                        error: function(response){
                            alert("Something went to wrong. Please try again later");
                        }
                    });
                }else{
                    console.log("error");
                }
                //nextStepWizard.removeAttr('disabled').trigger('click');
            };
        });

$('div.setup-panel div a.btn-success').trigger('click');
});
</script>
@stop
