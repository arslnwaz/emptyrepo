<form action="#" method="POST">
	<div class="uk-text-left" uk-grid>
		<div class="uk-width-1-1">
			<div>
				<label class="uk-form-label">Server</label>
				<div class="uk-form-controls">
					<input class="uk-input" type="text" placeholder="Server" value="{{isset($site->server->app_name)?$site->server->app_name:''}}" disabled="">
				</div>
			</div>
			<div>
				<label class="uk-form-label">Name</label>
				<div class="uk-form-controls">
					<input class="uk-input" type="text" placeholder="Name" value="{{isset($site->name)?$site->name:''}}" disabled="">
				</div>
			</div>
			<div>
				<label class="uk-form-label">Domain</label>
				<div class="uk-form-controls">
					<input class="uk-input" type="text" placeholder="Domain" value="{{isset($site->domain)?$site->domain:''}}" disabled="">
				</div>
			</div>
			<div>
				<label class="uk-form-label">Site Status</label>
				<div class="uk-form-controls">
					<input class="uk-input" type="text" placeholder="Site Status" value="{{isset($site->site_status)?$site->site_status:''}}" disabled="">
				</div>
			</div>
			<div>
				<label class="uk-form-label">User Name</label>
				<div class="uk-form-controls">
					<input class="uk-input" type="text" placeholder="User Name" value="{{isset($site->username)?$site->username:''}}" disabled="">
				</div>
			</div>
			<div>
				<label class="uk-form-label">Repository</label>
				<div class="uk-form-controls">
					<input class="uk-input" type="text" placeholder="Repository" value="{{isset($site->repository)?$site->repository:''}}" disabled="">
				</div>
			</div>
			<div>
				<label class="uk-form-label">Repository Provider</label>
				<div class="uk-form-controls">
					<input class="uk-input" type="text" placeholder="Repository Provider" value="{{isset($site->repository_provider)?$site->repository_provider:''}}" disabled="">
				</div>
			</div>
			<div>
				<label class="uk-form-label">Repository Branch</label>
				<div class="uk-form-controls">
					<input class="uk-input" type="text" placeholder="Repository Branch" value="{{isset($site->repository_branch)?$site->repository_branch:''}}" disabled="">
				</div>
			</div>
			<div>
				<label class="uk-form-label">Repository Status</label>
				<div class="uk-form-controls">
					<input class="uk-input" type="text" placeholder="Repository Status" value="{{isset($site->repository_status)?$site->repository_status:''}}" disabled="">
				</div>
			</div>
			<div>
				<label class="uk-form-label">Quick Deploy</label>
				<div class="uk-form-controls">
					<input class="uk-input" type="text" placeholder="Quick Deploy" value="{{isset($site->quick_deploy)?$site->quick_deploy:''}}" disabled="">
				</div>
			</div>
			<div>
				<label class="uk-form-label">Deployment Status</label>
				<div class="uk-form-controls">
					<input class="uk-input" type="text" placeholder="Deployment Status" value="{{isset($site->deployment_status)?$site->deployment_status:''}}" disabled="">
				</div>
			</div>
			<div>
				<label class="uk-form-label">Deployment Status</label>
				<div class="uk-form-controls">
					<input class="uk-input" type="text" placeholder="Deployment Status" value="{{isset($site->deployment_status)?$site->deployment_status:''}}" disabled="">
				</div>
			</div>
			<div>
				<label class="uk-form-label">Deployment Script</label>
				<div class="uk-form-controls">
					<Textarea class="uk-input" placeholder="Deployment Script" disabled="">{{isset($site->deployment_script)?$site->deployment_script:''}}</Textarea>
				</div>
			</div>
			<div>
				<label class="uk-form-label">Env File</label>
				<div class="uk-form-controls">
					<Textarea class="uk-input" placeholder="Env File" disabled="">{{isset($site->env_file)?$site->env_file:''}}</Textarea>
				</div>
			</div>
			<div>
				<label class="uk-form-label">App Status</label>
				<div class="uk-form-controls">
					<input class="uk-input" type="text" placeholder="App Status" value="{{isset($site->app_status)?$site->app_status:''}}" disabled="">
				</div>
			</div>
			<div>
				<label class="uk-form-label">Created At</label>
				<div class="uk-form-controls">
					<input class="uk-input" type="text" placeholder="Created At" value="{{isset($site->created_at)?$site->created_at:''}}" disabled="">
				</div>
			</div>
			<a href="{{route('wave.site',['section'=>'list'])}}" class="uk-button uk-button-primary uk-margin-top">Back</button>
		</div>
	</div>
</form>
@section('javascript')
<script>
	$('document').ready(function () {
		//$('.toggleswitch').bootstrapToggle();
	});
</script>
@endsection