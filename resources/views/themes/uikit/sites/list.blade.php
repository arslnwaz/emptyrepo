<form action="{{ route('wave.settings.api.post') }}" method="POST">
	<div class="uk-text-left" uk-grid>
		<div class="uk-width-1-1">
			<div>
				<div class="uk-form-controls">
					<a href="{{route('wave.site.add')}}"  class="uk-button uk-button-primary uk-align-right uk-margin-small-top">Create Site</a>
				</div>
			</div>
		</div>
	</div>
	{{ csrf_field() }}
</form>
@if(count($sites) > 0)
<h4>Sites</h4>

<table class="uk-table uk-table-striped">
	<thead>
		<tr>
			<th>Name</th>
			<th>Status</th>
			<th>Created</th>
			<th></th>
		</tr>
	</thead>
	<tbody>

		@foreach($sites as $site)
		<tr>
			<td>{{ $site->name }}</td>
			<td>{{ ($site->is_ready)?'Active':'Pending' }}</td>
			<td>{{ $site->created_at }}</td>
			<td>
				<button class="uk-button uk-button-default uk-button-small deletesite" data="{{$site->id}}" data-name="{{ $site->name }}" id="deleteSiteId"><span uk-icon="trash"></span></button>
				<a href="{{route('wave.site.view',['site'=>$site->id,'viewType'=>'view'])}}" class="uk-button uk-button-default uk-button-small viewapikey"><span uk-icon="info"></span></a>
				@if($site->site_status == "installed")
				<?php /*<button class="uk-button uk-button-default uk-button-small deploysite" data="{{ $site->id }}" data-name="{{ $site->name }}" id="deploySiteId" uk-toggle="target: #deploySite">Deploy</button> */ ?>
				<button class="uk-button uk-button-default uk-button-small viewsite" data="{{ $site->id }}" data-name="{{ $site->name }}" id="deployScriptId" uk-toggle="target: #deployScript">Deploy Script</button>
				<button class="uk-button uk-button-default uk-button-small deploynow" data="{{$site->id}}" data-name="{{ $site->name }}" id="deployId">Deplo Now</button>
				<button class="uk-button uk-button-default uk-button-small envfile" data="{{ $site->id }}" data-name="{{ $site->name }}" id="envId" uk-toggle="target: #envFile">Env File</button>
				@endif
			</td>
		</tr>
		@endforeach
	</tbody>
</table>

<div id="deployNow" uk-modal>
	<div class="uk-modal-dialog uk-modal-body">
		<h2 class="uk-modal-title">Deploy Now</h2>
		<form action="" id="editKeyForm" method="POST">
			<input type="text" class="uk-input" name="key_name" id="editKeyInput">
			<input type="hidden" name="_method" value="PUT">
			<p class="uk-text-right">
				<button class="uk-button uk-button-default uk-modal-close">Cancel</button>
				<button class="uk-button uk-button-primary" type="submit">Update</button>
			</p>
			{{ csrf_field() }}
		</form>
	</div>
</div>
<div id="deployScript" uk-modal>
	<div class="uk-modal-dialog uk-modal-body">
		<h2 class="uk-modal-title">Deploy Script</h2>
		<form action="#" id="deployScriptForm" method="POST">
			<input type="hidden" name="id" id="siteID" value="">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<div class="uk-margin">
				<label for="name">Script</label>
				<textarea  class="uk-textarea" id="deployment_script" name="deployment_script" rows="10" required="">

				</textarea>
			</div>
			<p class="uk-text-right">
				<button class="uk-button uk-button-default uk-modal-close">Cancel</button>
				<button class="uk-button uk-button-danger uk-text-white" type="submit">Save</button>
			</p>
			{{ csrf_field() }}
		</form>
	</div>
</div>

<div id="envFile" uk-modal>
	<div class="uk-modal-dialog uk-modal-body">
		<h2 class="uk-modal-title">Env File</h2>
		<form action="#" id="envFileForm" method="POST">
			<input type="hidden" name="id" id="siteID" value="">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<div class="uk-margin">
				<label for="name">Env File</label>
				<textarea  class="uk-textarea" id="env_file" name="env_file" rows="10" required="">

				</textarea>
			</div>
			<p class="uk-text-right">
				<button class="uk-button uk-button-default uk-modal-close">Cancel</button>
				<button class="uk-button uk-button-danger uk-text-white" type="submit">Save</button>
			</p>
			{{ csrf_field() }}
		</form>
	</div>
</div>

<div id="deleteSite" uk-modal>
	<div class="uk-modal-dialog uk-modal-body">
		<h2 class="uk-modal-title">Delete this site?</h2>
		<p>Are you sure you want to delete site: <code><span id="deleteSiteName"></span></code></p>
		<form action="" id="deleteSiteForm" method="POST">
			<input type="hidden" name="_method" value="DELETE">
			<p class="uk-text-right">
				<button class="uk-button uk-button-default uk-modal-close">Cancel</button>
				<button class="uk-button uk-button-danger uk-text-white" type="submit">Delete</button>
			</p>
			{{ csrf_field() }}
		</form>
	</div>
</div>

@else
<p class="uk-text-center">No API Keys Created Yet.</p>
@endif
@section('javascript')
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.19.0/jquery.validate.min.js"></script>
<script>
	$(document).ready(function () {

		UIkit.util.on('#deleteSiteId', 'click', function (e) {
			e.preventDefault();
			e.target.blur();
			var id=$(this).attr("data");
			var name=$(this).attr("data-name");
			var url="{{route('wave.site.delete')}}/"+id;
			UIkit.modal.confirm('Are you sure to delete site :'+name+'?').then(function () {
				var data1 = new FormData();
				data1.append('_token',"{{ csrf_token() }}");
				$.ajax({
					type: "POST",
					url: url,
					data: data1,
					processData: false,
					contentType: false,
					success: function(response){
						if(response.hasOwnProperty('status') && response.status == true){
							location.reload();
						}else if(response.hasOwnProperty('message')){
							alert(response.message);
						}else{
							alert("Something went to wrong. Please try again later");
						}
                    	//nextStepWizard.removeAttr('disabled').trigger('click');
                    },
                    error: function(response){
                    	alert("Something went to wrong. Please try again later");
                    }
                });
			}, function () {
				//console.log('Rejected.')
			});
		});


		UIkit.util.on('#deployId', 'click', function (e) {
			e.preventDefault();
			e.target.blur();
			var id=$(this).attr("data");
			var name=$(this).attr("data-name");
			var url="{{route('wave.site.deploy.now')}}/"+id;
			UIkit.modal.confirm('Are you want to deploy script now?').then(function () {
				var data1 = new FormData();
				data1.append('_token',"{{ csrf_token() }}");
				$.ajax({
					type: "GET",
					url: url,
					processData: false,
					contentType: false,
					success: function(response){
						if(response.hasOwnProperty('status') && response.status == true){
							alert(response.message);
						}else if(response.hasOwnProperty('message')){
							alert(response.message);
						}else{
							alert("Something went to wrong. Please try again later");
						}
                    	//nextStepWizard.removeAttr('disabled').trigger('click');
                    },
                    error: function(response){
                    	alert("Something went to wrong. Please try again later");
                    }
                });
			}, function () {
				//console.log('Rejected.')
			});
		});

		UIkit.util.on('#deployScriptId', 'click', function (e) {
			e.preventDefault();
			var id=$(this).attr("data");
			var url="{{route('wave.site.deploy.script')}}/"+id;
			$.ajax({
				type: "get",
				url: url,
				processData: false,
				contentType: false,
				success: function(response){
					if(response.hasOwnProperty('status') && response.status == true){
						$("#deployment_script").val(response.deployment_script);
					}else if(response.hasOwnProperty('message')){
						alert(response.message);
					}else{
						alert("Something went to wrong. Please try again later");
					}
				},
				error: function(response){
					alert("Something went to wrong. Please try again later");
				}
			});

			$("#deployScriptForm").on("submit",function(e){
				e.preventDefault();
				if($("#deployScriptForm").validate()){
					$("#deployScriptForm #siteID").val(id);
					var url="{{route('wave.site.deploy.script')}}/"+id;
					var data = new FormData($("#deployScriptForm")[0]);
					$.ajax({
						type: "POST",
						url: url,
						data:data,
						//dataType:"json",
						processData: false,
						contentType: false,
						success: function(response){
							if(response.hasOwnProperty('status') && response.status == true){
								alert(response.message);
							}else if(response.hasOwnProperty('message')){
								alert(response.message);
							}else{
								alert("Something went to wrong. Please try again later");
							}
						},
						error: function(response){
							alert("Something went to wrong. Please try again later");
						}
					});					
				}else{
					alert("Please enter deploy script");
				}
			});

		});

		UIkit.util.on('#envId', 'click', function (e) {
			e.preventDefault();
			var id=$(this).attr("data");
			var url="{{route('wave.site.env.update')}}/"+id;
			$.ajax({
				type: "get",
				url: url,
				processData: false,
				contentType: false,
				success: function(response){
					if(response.hasOwnProperty('status') && response.status == true){
						$("#env_file").val(response.env_file);
					}else if(response.hasOwnProperty('message')){
						alert(response.message);
					}else{
						alert("Something went to wrong. Please try again later");
					}
				},
				error: function(response){
					alert("Something went to wrong. Please try again later");
				}
			});

			$("#envFileForm").on("submit",function(e){
				e.preventDefault();
				if($("#envFileForm").validate()){
					$("#envFileForm #siteID").val(id);
					var url="{{route('wave.site.env.update')}}/"+id;
					var data = new FormData($("#envFileForm")[0]);
					$.ajax({
						type: "POST",
						url: url,
						data:data,
						//dataType:"json",
						processData: false,
						contentType: false,
						success: function(response){
							if(response.hasOwnProperty('status') && response.status == true){
								alert(response.message);
							}else if(response.hasOwnProperty('message')){
								alert(response.message);
							}else{
								alert("Something went to wrong. Please try again later");
							}
						},
						error: function(response){
							alert("Something went to wrong. Please try again later");
						}
					});					
				}else{
					alert("Please enter deploy script");
				}
			});

		});
	});
</script>
@endsection