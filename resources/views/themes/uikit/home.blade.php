@extends('theme::layouts.app')

@section('content')

<div class="uk-section-default uk-section uk-flex uk-flex-middle uk-padding-small uk-padding-remove-bottom uk-margin-small-top" style="box-sizing: border-box;">
	<div class="uk-width-1-1">
		<div class="uk-container">

			<div class="uk-grid-large uk-flex-middle uk-grid-margin-large uk-grid uk-padding-bottom@m" uk-grid="">

				<div class="uk-width-expand@m uk-first-column uk-animation-slide-left-small">

					<h1 class="uk-text-left@m uk-text-center uk-h6 uk-scrollspy-inview uk-animation-slide-left-medium" uk-scrollspy-class="">{{ theme('home_headline') }}</h1>
					<h2 class="uk-width-xlarge uk-margin-auto uk-text-left@m uk-text-center uk-h1 uk-scrollspy-inview uk-animation-slide-left-medium" uk-scrollspy-class="">{{ theme('home_subheadline') }}</h2>
					<div class="uk-margin uk-text-left@m uk-text-center uk-scrollspy-inview uk-animation-slide-left-medium" uk-scrollspy-class="">{{ theme('home_description') }}</div>
					<form class="flex" action="https://rapidstartup.us10.list-manage.com/subscribe/post?u=c31ad731170a2894230694b1c&amp;id=a44c39c36f" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank" novalidate>
                    	<input class="uk-input required email" type="email" placeholder="Your Email Address" value="" name="EMAIL" id="mce-EMAIL">
 						<input type="hidden" name="b_c31ad731170a2894230694b1c_a44c39c36f" tabindex="-1" value="">
						<button class="uk-margin uk-text-left@m uk-text-center uk-scrollspy-inview uk-animation-slide-left-medium el-content uk-button uk-button-primary">YES, I WANT IN!</button>
					</form>
					<!-- <div class="uk-margin-medium uk-text-left@m uk-text-center uk-scrollspy-inview uk-animation-slide-left-medium" uk-scrollspy-class="" style="">
						<a class="el-content uk-button uk-button-primary" href="{{ theme('home_cta_url') }}" title="{{ theme('home_cta') }}">
							{{ theme('home_cta') }}
						</a>
					</div> -->

				</div>

				<div class="uk-width-expand@m uk-animation-slide-right-small">

					<div class="uk-margin uk-text-center uk-scrollspy-inview uk-animation-slide-right" uk-scrollspy-class="uk-animation-slide-right" style="">
						<img src="{{ Voyager::image(theme('home_promo_image')) }}" class="el-image" alt="" style="max-height:550px">
					</div>

				</div>
			</div>
		</div>

	</div>

</div>

<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 1440 126" style="enable-background:new 0 0 1440 126;" xml:space="preserve">
	<style type="text/css">
		/*.wave-svg{fill:#1683FB;}*/
		.wave-svg{fill:#65D5E3;}
	</style>
	<g id="wave" transform="translate(720.000000, 75.000000) scale(1, -1) translate(-720.000000, -75.000000) " fill-rule="nonzero">
        <path class="wave-svg" d="M694,94.437587 C327,161.381336 194,153.298248 0,143.434189 L2.01616501e-13,44.1765618 L1440,27 L1440,121 C1244,94.437587 999.43006,38.7246898 694,94.437587 Z" id="Shape" fill="#0069FF" opacity="0.519587054"></path>
        <path class="wave-svg" d="M686.868924,95.4364002 C416,151.323752 170.73341,134.021565 1.35713663e-12,119.957876 L0,25.1467017 L1440,8 L1440,107.854321 C1252.11022,92.2972893 1034.37894,23.7359827 686.868924,95.4364002 Z" id="Shape" fill="#0069FF" opacity="0.347991071"></path>
        <path class="wave-svg" d="M685.6,30.8323303 C418.7,-19.0491687 170.2,1.94304528 0,22.035593 L0,118 L1440,118 L1440,22.035593 C1252.7,44.2273621 1010,91.4098622 685.6,30.8323303 Z" id="Shape" fill="url(#linearGradient-1)" transform="translate(720.000000, 59.000000) scale(1, -1) translate(-720.000000, -59.000000) "></path>
    </g>
</svg>
@php 
if(theme('home_hide_show_features_section') == '1' && count($features)>0)
{
@endphp
<div class="uk-width-1-1" id="features">
	<div class="uk-container">
		<h2>{{ theme('home_features_heading') }}</h2>
		<p class="uk-text-center uk-margin-remove-top uk-align-center uk-margin-remove-bottom" style="color:#ffffff; opacity:0.8;">{{ theme('home_features_sub_heading') }}</p>
		<div class="uk-grid">
			@foreach($features as $feature)

			<div class="uk-width-1-3@m uk-width-1-2@s uk-width-1-1 uk-margin-large-top uk-text-center" uk-scrollspy="cls:uk-animation-slide-bottom">
				<img src="{{ url('storage/'.$feature->feature_image) }}">
				<h4>{{ $feature->title }}</h4>
				<p>{!! $feature->description !!}</p>
			</div>
			@endforeach

		</div>
	</div>
</div>
@php
}else{
@endphp
<div class="uk-width-1-1" id="features">
	<div class="uk-container">
		<h2>{{ !empty(theme('home_features_heading')) ? theme('home_features_heading') : 'Check Out The Awesome App Features' }}</h2>
		<p class="uk-text-center uk-margin-remove-top uk-align-center uk-margin-remove-bottom" style="color:#ffffff; opacity:0.8;">{{ !empty(theme('home_features_sub_heading')) ? theme('home_features_sub_heading') : 'RapidStartup.io has all the features you need to help you rapidly launch your Software as a Service App.<br> Here are the core features we\'ve pre-rolled for you so you can grow and scale without limits. <br/> No more hacking and mashing together a bunch of expensive apps to get the things you need to succeed!</p>' }}
		<div class="uk-grid">
			<div class="uk-width-1-3@m uk-width-1-2@s uk-width-1-1 uk-margin-large-top uk-text-center" uk-scrollspy="cls:uk-animation-slide-bottom">
				<img src="{{ theme_folder_url('/images/authentication.png') }}">
				<h4>Authentication</h4>
				<p>Fully loaded authentication, email verification, and password reset. Authentication in a snap!</p>
			</div>

			<div class="uk-width-1-3@m uk-width-1-2@s uk-width-1-1 uk-margin-medium-top uk-margin-medium-bottom uk-text-center" uk-scrollspy="cls:uk-animation-slide-bottom">
				<img src="{{ theme_folder_url('/images/themes.png') }}">
				<h4>Customize Your Look</h4>
				<p>Fully configurable theme. Update your logo, name/titles, description, primary color, icons, and more...</p>
			</div>
			
			<div class="uk-width-1-3@m uk-width-1-2@s uk-width-1-1 uk-margin-large-top uk-text-center" uk-scrollspy="cls:uk-animation-slide-bottom">
				<img src="{{ theme_folder_url('/images/profile.png') }}">
				<h4>User Profiles</h4>
				<p>Customizable user profiles. Allow your users to enter data and easily customize their user profiles.</p>
			</div>

			<div class="uk-width-1-3@m uk-width-1-2@s uk-width-1-1 uk-margin-large-top uk-text-center" uk-scrollspy="cls:uk-animation-slide-bottom">
				<img src="{{ theme_folder_url('/images/impersonation.png') }}">
				<h4>User Impersonation</h4>
				<p>With user impersonations you can login as another user and resolve an issue or troubleshoot a bug.</p>
			</div>

			<div class="uk-width-1-3@m uk-width-1-2@s uk-width-1-1 uk-margin-medium-top uk-text-center" uk-scrollspy="cls:uk-animation-slide-bottom">
				<img src="{{ theme_folder_url('/images/subscriptions.png') }}">
				<h4>Subscriptions</h4>
				<p>Allow users to pay for your service and signup for a subscription using Stripe or Braintree Payments.</p>
			</div>

			<div class="uk-width-1-3@m uk-width-1-2@s uk-width-1-1 uk-margin-medium-top uk-text-center" uk-scrollspy="cls:uk-animation-slide-bottom">
				<img src="{{ theme_folder_url('/images/roles.png') }}">
				<h4>User Roles</h4>
				<p>Grant user permissions based on roles, you can then assign a role to a specific plan.</p>
			</div>

			<div class="uk-width-1-3@m uk-width-1-2@s uk-width-1-1 uk-margin-medium-top uk-text-center" uk-scrollspy="cls:uk-animation-slide-bottom">
				<img src="{{ theme_folder_url('/images/notifications.png') }}">
				<h4>Popup Notifications</h4>
				<p>Ready-to-use Notification System to keep your users informed about their account or history.</p>
			</div>

			<div class="uk-width-1-3@m uk-width-1-2@s uk-width-1-1 uk-margin-medium-top uk-text-center" uk-scrollspy="cls:uk-animation-slide-bottom">
				<img src="{{ theme_folder_url('/images/announcements.png') }}">
				<h4>Announcements</h4>
				<p>Create user announcements to notify users about new features or updates in your application.</p>
			</div>

			<div class="uk-width-1-3@m uk-width-1-2@s uk-width-1-1 uk-margin-medium-top uk-margin-medium-bottom uk-text-center" uk-scrollspy="cls:uk-animation-slide-bottom">
				<img src="{{ theme_folder_url('/images/api.png') }}">
				<h4>Fully Functional API</h4>
				<p>Create API connections for your application. Ready to integrate into other applications you have.</p>
			</div>


		</div>
	</div>
</div>
@php
}
@endphp

@if(setting('site.placement') == 'features')
	<div id="custom_section">
		{!! html_entity_decode(setting('site.custom_section')) !!}
	</div>
@endif

<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 1440 156" style="enable-background:new 0 0 1440 126;" xml:space="preserve">
	<style type="text/css">
		.wave-svg{fill:#DA4B54;}
	</style>
	<g id="wave" fill-rule="nonzero">
        <path class="wave-svg" d="M694,94.437587 C327,161.381336 194,153.298248 0,143.434189 L2.01616501e-13,44.1765618 L1440,27 L1440,121 C1244,94.437587 999.43006,38.7246898 694,94.437587 Z" id="Shape" fill="#0069FF" opacity="0.519587054"></path>
        <path class="wave-svg" d="M686.868924,95.4364002 C416,151.323752 170.73341,134.021565 1.35713663e-12,119.957876 L0,25.1467017 L1440,8 L1440,107.854321 C1252.11022,92.2972893 1034.37894,23.7359827 686.868924,95.4364002 Z" id="Shape" fill="#0069FF" opacity="0.347991071"></path>
        <path class="wave-svg" d="M685.6,30.8323303 C418.7,-19.0491687 170.2,1.94304528 0,22.035593 L0,118 L1440,118 L1440,22.035593 C1252.7,44.2273621 1010,91.4098622 685.6,30.8323303 Z" id="Shape" fill="url(#linearGradient-1)" transform="translate(720.000000, 59.000000) scale(1, -1) translate(-720.000000, -59.000000) "></path>
    </g>
</svg>

@php 
if(theme('home_hide_show_testimonials_section') == '1' && count($testimonials)>0)
{
@endphp
<div id="testimonials">
	<div class="uk-container uk-margin-small-top">
		<h2>{{ theme('home_testimonials_heading') }}</h2>
		<p class="uk-text-center uk-margin-remove-top">{{ theme('home_testimonials_sub_heading') }}</p>

			<div class="uk-position-relative uk-visible-toggle uk-dark" uk-slider="autoplay: true;">
				<ul class="uk-slider-items uk-child-width-1-1">

					@foreach($testimonials as $testimonial)
						<li class="testimonial uk-text-center">
				            <img src="{{ url('storage/'.$testimonial->client_image) }}" alt="{{ $testimonial->alt_text}}" uk-slider-parallax="x: 200,-200">
				            <p uk-slider-parallax="x: 200,-200">{{ $testimonial->name }}</p>
							<blockquote uk-slider-parallax="x: 100,-100">{{ $testimonial->description }}</blockquote>
				        </li>
					@endforeach
			    </ul>

		    	<a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
    			<a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
            	<ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
            </div>
	</div>
</div>

@php
}else{
@endphp

<div id="testimonials">
	<div class="uk-container uk-margin-small-top">
		<h2>{{ !empty(theme('home_testimonials_heading')) ? theme('home_testimonials_heading') : 'RapidStartup Testimonials' }}</h2>
		<p class="uk-text-center uk-margin-remove-top">{{ !empty(theme('home_testimonials_sub_heading')) ? theme('home_testimonials_sub_heading') : 'Here are some happy customers building out their SAAS' }}</p>

			<div class="uk-position-relative uk-visible-toggle uk-dark" uk-slider="autoplay: true;">
				<ul class="uk-slider-items uk-child-width-1-1">
			        <li class="testimonial uk-text-center">
			            <img src="{{ theme_folder_url('/images/testimonial-1.jpg') }}" alt="Testimonial 1" uk-slider-parallax="x: 200,-200">
			            <p uk-slider-parallax="x: 200,-200">Rachel M.</p>
						<blockquote uk-slider-parallax="x: 100,-100">Using RapidStartup I was able to quickly create my app and get it shared with my clients. It was so easy to get started and use. Now, I can focus on closing the leads it's generating.</blockquote>
			        </li>
			        <li class="testimonial uk-text-center">
			            <img src="{{ theme_folder_url('/images/testimonial-2.jpg') }}" alt="Testimonial 2" uk-slider-parallax="x: 200,-200">
			            <p uk-slider-parallax="x: 200,-200">Nick V.</p>
						<blockquote uk-slider-parallax="x: 100,-100">Creating an automated tool to perform some of the tasks that I did manually for my agency clients saves me a tonne of time and money.<br> With RapidStartup, I was able to save myself hundreds of hours of work!</blockquote>
			        </li>
			    </ul>

		    	<a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
    			<a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
            	<ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
            </div>
	</div>
</div>
@php
}
@endphp

@if(setting('site.placement') == 'testimonials')
	<div id="custom_section">
		{!! html_entity_decode(setting('site.custom_section')) !!}
	</div>
@endif

<svg version="1.1" id="Layer_3" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 1440 126" style="enable-background:new 0 0 1440 126;" xml:space="preserve">
	<style type="text/css">
		.wave-svg-light{fill:#DA4B54;}
	</style>
	<g id="wave" transform="translate(720.000000, 75.000000) scale(1, -1) translate(-720.000000, -75.000000) " fill-rule="nonzero">
        <path class="wave-svg-light" d="M694,94.437587 C327,161.381336 194,153.298248 0,143.434189 L2.01616501e-13,44.1765618 L1440,27 L1440,121 C1244,94.437587 999.43006,38.7246898 694,94.437587 Z" id="Shape" fill="#0069FF" opacity="0.519587054"></path>
        <path class="wave-svg-light" d="M686.868924,95.4364002 C416,151.323752 170.73341,134.021565 1.35713663e-12,119.957876 L0,25.1467017 L1440,8 L1440,107.854321 C1252.11022,92.2972893 1034.37894,23.7359827 686.868924,95.4364002 Z" id="Shape" fill="#0069FF" opacity="0.347991071"></path>
        <path class="wave-svg-light" d="M685.6,30.8323303 C418.7,-19.0491687 170.2,1.94304528 0,22.035593 L0,118 L1440,118 L1440,22.035593 C1252.7,44.2273621 1010,91.4098622 685.6,30.8323303 Z" id="Shape" fill="url(#linearGradient-1)" transform="translate(720.000000, 59.000000) scale(1, -1) translate(-720.000000, -59.000000) "></path>
    </g>
</svg>

<div id="pricing">
	<div class="uk-container">
		<h2 class="uk-margin-top-large">SAAS Platform Pricing</h2>
		<p class="uk-text-center uk-margin-remove-top uk-margin-medium-bottom">It's easy to customize your own pricing of your Software as a Service</p>

		@php $plans = Wave\Plan::where('active_on_homepage','=',1)->get() @endphp
		<div class="uk-child-width-1-{{ count($plans) }}@m uk-grid-small uk-grid-match" uk-grid>
	 	    @foreach($plans as $plan)
		    	@php $features = explode(',', $plan->features); @endphp
		    	<div>
			        <div class="uk-card uk-card-default uk-card-body uk-card-plan @if($plan->default){{ 'selected' }}@endif">
			            <h3 class="uk-card-title uk-text-center">{{ $plan->name }}</h3>
			            <p class="uk-text-center uk-text-muted">{{ $plan->price }}</p>
			            <ul class="uk-padding-remove-left uk-text-center uk-list">
			            	@foreach($features as $feature)
			            		<li><span class="uk-margin-small-right" uk-icon="check"></span> {{ $feature }}</li>
			            	@endforeach
			            </ul>
			            <div class="uk-text-center">
			            	<a class="uk-button uk-button-primary" href="{{ theme('home_cta_url') }}">Select Plan</a>
			            </div>
			        </div>
			    </div>
	 		@endforeach
	 	</div>

	 	<p class="uk-text-center uk-margin-medium"><span class="uk-margin-small-right" uk-icon="settings"></span>Plans are fully configurable for your own SAAS in your Admin Area.</p>

	</div>
</div>

@if(setting('site.placement') == 'pricing')
	<div id="custom_section">
		{!! html_entity_decode(setting('site.custom_section')) !!}
	</div>
@endif

@endsection