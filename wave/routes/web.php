<?php

Route::impersonate();

Route::get('/', '\Wave\Http\Controllers\HomeController@index')->name('wave.home');
Route::get('@{username}', '\Wave\Http\Controllers\ProfileController@index')->name('wave.profile');

// Additional Auth Routes
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('user/verify/{verification_code}', 'Auth\RegisterController@verify')->name('verify');
Route::post('register/subscribe', '\Wave\Http\Controllers\RegisterController@register')->name('wave.register-subscribe');

Route::get('blog', '\Wave\Http\Controllers\BlogController@index')->name('wave.blog');
Route::get('blog/{category}', '\Wave\Http\Controllers\BlogController@category')->name('wave.blog.category');
Route::get('blog/{category}/{post}', '\Wave\Http\Controllers\BlogController@post')->name('wave.blog.post');

/***** Pages *****/
Route::get('p/{page}', '\Wave\Http\Controllers\PageController@page');

/***** General Pages *****/
/*Route::get('support','\Wave\Http\Controllers\SupportController')->name('wave.support');*/
/*Route::view('support','support')->name('support');
Route::post('support', '\Wave\Http\Controllers\SupportController@send')->name('support.send');*/

/***** Billing Webhook *****/
Route::post('/billing/webhook', '\Wave\Http\Controllers\WebhookController@handleWebhook');

Route::group(['middleware' => 'wave'], function () {
	Route::get('dashboard', '\Wave\Http\Controllers\DashboardController@index')->name('wave.dashboard');
});

Route::group(['middleware' => 'auth'], function(){
	Route::get('settings/{section?}', '\Wave\Http\Controllers\SettingsController@index')->name('wave.settings');

	Route::post('settings/profile', '\Wave\Http\Controllers\SettingsController@profilePut')->name('wave.settings.profile.put');
	Route::put('settings/security', '\Wave\Http\Controllers\SettingsController@securityPut')->name('wave.settings.security.put');

	Route::post('settings/api', '\Wave\Http\Controllers\SettingsController@apiPost')->name('wave.settings.api.post');
	Route::put('settings/api/{id}', '\Wave\Http\Controllers\SettingsController@apiPut')->name('wave.settings.api.put');
	Route::delete('settings/api/{id}', '\Wave\Http\Controllers\SettingsController@apiDelete')->name('wave.settings.api.delete');

	/*User Site*/
	Route::get('site/index/{section?}', '\Wave\Http\Controllers\SitesController@index')->name('wave.site');
	Route::get('site/add', '\Wave\Http\Controllers\SitesController@create')->name('wave.site.add');
	//Route::post('site/create', '\Wave\Http\Controllers\SitesController@apiPost')->name('wave.site.create');
	Route::post('site/delete/{id?}', '\Wave\Http\Controllers\SitesController@delete')->name('wave.site.delete');
	Route::get('site/view/{id}/{viewType?}', '\Wave\Http\Controllers\SitesController@view')->name('wave.site.view');
	
	//Site Deploy repo
   	Route::get('site-deploy/{id?}', ['uses' => '\Wave\Http\Controllers\SitesController@deployView', 'as' => 'wave.site.deploy']);
   	Route::post('site-deploy/{id?}', ['uses' => '\Wave\Http\Controllers\SitesController@deploy', 'as' => 'wave.site.deploy']);


   	//Site Deploy Script
   	Route::get('site-deploy-script/{id?}', ['uses' => '\Wave\Http\Controllers\SitesController@deployScriptView', 'as' => 'wave.site.deploy.script']);
   	Route::post('site-deploy-script/{id?}', ['uses' => '\Wave\Http\Controllers\SitesController@deployScript', 'as' => 'wave.site.deploy.script']);

   	//Deploy Now
   	Route::get('site-deploy-now/{id?}', ['uses' => '\Wave\Http\Controllers\SitesController@deployNow', 'as' => 'wave.site.deploy.now']);

	//Site Env File
   	Route::get('site-env/{id?}', ['uses' => '\Wave\Http\Controllers\SitesController@envView', 'as' => 'wave.site.env.update']);
   	Route::post('site-env/{id?}', ['uses' => '\Wave\Http\Controllers\SitesController@envUpdate', 'as' => 'wave.site.env.update']);   	

   	//seite save create form ajax request in create site
    Route::post('site-create-ajax', ['uses' => '\Wave\Http\Controllers\SitesController@createAjax', 'as' => 'wave.site.create.ajax']);
    Route::post('site-install-repo-ajax', ['uses' => '\Wave\Http\Controllers\SitesController@repoAjax', 'as' => 'wave.site.install.repo.ajax']);
    Route::post('site-get_deploy-script-ajax', ['uses' => '\Wave\Http\Controllers\SitesController@getDeployScriptAjax', 'as' => 'wave.site.get.deploy.script.ajax']);
    Route::post('site-deploy-script-ajax', ['uses' => '\Wave\Http\Controllers\SitesController@deployScriptAjax', 'as' => 'wave.site.deploy.script.ajax']);
    Route::post('site-get-env-ajax', ['uses' => '\Wave\Http\Controllers\SitesController@getEnvAjax', 'as' => 'wave.site.get.env.ajax']);
    Route::post('site-update-env-ajax', ['uses' => '\Wave\Http\Controllers\SitesController@updateEnvAjax', 'as' => 'wave.site.env.ajax']);



	Route::get('settings/invoices/{invoice}', '\Wave\Http\Controllers\SettingsController@invoice')->name('wave.invoice');

	Route::get('notifications', '\Wave\Http\Controllers\NotificationController@index')->name('wave.notifications');
	Route::get('announcements', '\Wave\Http\Controllers\AnnouncementController@index')->name('wave.announcements');
	Route::get('announcement/{id}', '\Wave\Http\Controllers\AnnouncementController@announcement')->name('wave.announcement');
	Route::post('announcements/read', '\Wave\Http\Controllers\AnnouncementController@read')->name('wave.announcements.read');
	Route::get('notifications', '\Wave\Http\Controllers\NotificationController@index')->name('wave.notifications');
	Route::post('notification/read/{id}', '\Wave\Http\Controllers\NotificationController@delete')->name('wave.notification.read');


	//Route::get('socialposts', '\Wave\Http\Controllers\SocialpostController@index')->name('wave.socialposts');
	//Route::get('socialpost/{id}', '\Wave\Http\Controllers\SocialpostController@socialpost')->name('wave.socialpost');
	//Route::post('socialposts/read', '\Wave\Http\Controllers\SocialpostController@read')->name('wave.socialposts.read');

	Route::post('subscribe', '\Wave\Http\Controllers\SubscriptionController@subscribe')->name('wave.subscribe');
	Route::get('subscription/cancel', '\Wave\Http\Controllers\SubscriptionController@cancel')->name('wave.cancel');
	Route::get('subscription/reactivate', '\Wave\Http\Controllers\SubscriptionController@reactivate')->name('wave.reactivate');
	Route::post('plans/update', '\Wave\Http\Controllers\SubscriptionController@update_plans')->name('wave.update_plan');
	Route::post('update_credit_card', '\Wave\Http\Controllers\SubscriptionController@update_credit_card')->name('wave.update_credit_card');
	Route::view('trial_over', 'theme::trial_over')->name('wave.trial_over');
	Route::view('cancelled', 'theme::cancelled')->name('wave.cancelled');
});