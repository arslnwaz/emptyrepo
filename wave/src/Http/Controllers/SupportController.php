<?php

namespace Wave\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
class SupportController extends \App\Http\Controllers\Controller
{
    public function send (Request $request) {
    	//$user = auth()>user();

        $message = $request->message;
        $subject = $request->subject;
        $email = $request->email;
        $data=array();
        $data["message"]=$message;
        $data["subject"]=$subject;
        $data["email"]=$email;
        if($message == "" || $subject == ""){
            return back()->with(['alert' => 'Please make sure to fill out subject and message', 'alert_type' => 'error'])
                         ->withInput($request->all());
        }
        Mail::send('mail.support',['data'=>$data], function($mail) use ($email) {
            $mail->from($email, "Namehere");
            $mail->to('help@rapidstartup.io')->subject('New Ticket');
        });
        return back()->with(['alert' => 'Successfully submitted your request. Please allow up to 24-48hrs for a response', 'alert_type' => 'success']);
    }
}