<?php

namespace Wave\Http\Controllers;

use Wave\Page;
use Illuminate\Http\Request;

class PageController extends \App\Http\Controllers\Controller
{
	public function page($slug){
		$page = Page::where('slug', '=', $slug)->firstOrFail();

		$seo = [
			'seo_title' => $page->title,
			'seo_description' => $page->meta_description,
		];

		if (isset($page->custom_header_code)) {
			$page->custom_header_code = strip_tags($page->custom_header_code, '<script>');
		}
		if (isset($page->custom_footer_code)) {
			$page->custom_footer_code = strip_tags($page->custom_footer_code, '<script>');
		}
		if (isset($page->custom_css)) {
			$page->custom_css = strip_tags($page->custom_css, '<style>');
		}
		
		return view('theme::page', compact('page', 'seo'));
	}
}
