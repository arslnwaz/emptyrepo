<?php

namespace Wave\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Redirect;
use Validator;
use Wave\User;
use Wave\KeyValue;
use Wave\ApiKey;
use App\UserApp;
use App\UserSite;
use TCG\Voyager\Http\Controllers\Controller;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\TooManyRedirectsException;
use GuzzleHttp\Psr7;

class SitesController extends Controller
{
    public function index($section = 'list'){
        $sites=UserSite::where("user_id",auth()->user()->id)->orderBy("created_at","desc")->get();
        return view('theme::sites.index', compact('section','sites'));
    }

    public function create($viewType="",Request $request)
    {
        $section="add";
        return view('theme::sites.index', compact('section'));
    }
    public function view($id,$viewType="",Request $request)
    {
        $site=UserSite::where("id",$id)->where("user_id",auth()->user()->id)->first();
        if(!isset($site->id)){
            return Voyager::view('wave.site.list');    
        }
        $site->server=UserApp::select("server_id","is_ready","app_name")->where("id",$site->server_id)->first();
        $section="view";
        return view('theme::sites.index',compact('site','section'));
    }
    public function delete($id,Request $request)
    {
        if(empty($id)){
            return response()->json(['message' => "Please select site to delete",'status'=>true]);exit;
        }
        $site = DB::table("user_sites")->where('id', $id)->where('user_id',auth()->user()->id)->first();
        if(isset($site->id)){

            /*Temp Test*/
            /*DB::table("user_sites")->where('id', $id)->where('user_id',auth()->user()->id)->delete();
            return response()->json(['message' => "Site deleted successfully",'status'=>true]);exit;*/
            try{
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                        //'Accept'=>'application/json',
                        //'Content-Type'=>'application/json'
                ];
                $body=[];
                $res = $client->request('GET', 'https://forge.laravel.com/api/v1/servers/'.$site->server_id.'/sites/'.$site->site_id, [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);
                if ($res->getStatusCode() == 200) { // 200 OK
                    $response_data = $res->getBody()->getContents();
                    $response_data=json_decode($response_data);
                    $request_data=array();
                    if(!isset($response_data->site->id)){
                        DB::table("user_sites")->where('id', $id)->where('user_id',auth()->user()->id)->delete();
                        return response()->json(['message' => "Site deleted successfully",'status'=>true]);exit;
                    }else{
                        /*delete site from sevre*/
                        $client = new Client();
                        $headers=[
                            'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                        //'Accept'=>'application/json',
                        //'Content-Type'=>'application/json'
                        ];
                        $body=[];
                        $res = $client->request('DELETE', 'https://forge.laravel.com/api/v1/servers/'.$site->server_id.'/sites/'.$site->site_id, [
                            'form_params' => $body, 
                            'headers' => $headers 
                        ]);     
                        if ($res->getStatusCode() == 200) { // 200 OK
                            /*$response_data = $res->getBody()->getContents();
                            $response_data=json_decode($response_data);
                            $request_data=array();
                            if(isset($response_data->servers) && in_array($site->site_id, $response_data->servers)){
                                DB::table("user_sites")->where('id', $id)->where('user_id',auth()->user()->id)->delete();
                                return response()->json(['message' => "Site deleted successfully",'status'=>true]);exit;
                            }else{
                                return response()->json(['message' => "Can not delete site from sever.",'status'=>false]);exit;
                            }*/
                            DB::table("user_sites")->where('id', $id)->where('user_id',auth()->user()->id)->delete();
                            return response()->json(['message' => "Site deleted successfully",'status'=>true]);exit;
                        }else{
                            return response()->json(['message' => "Can not delete site from sever.",'status'=>false]);exit;
                        }
                    }
                }
            }catch(ClientException $e){
                $responseBodyAsString = $e->getResponse()->getBody()->getContents();
                return response()->json(['message' => "Client exception : ".$responseBodyAsString,'status'=>false]);exit;
            }catch(RequestException $e){
                $responseBodyAsString = $e->getResponse()->getBody()->getContents();
                return response()->json(['message' => "Request exception",'status'=>false]);exit;
            }catch(BadResponseException $e){
                return response()->json(['message' => "Bad response exception",'status'=>false]);exit;
            }catch(ServerException $e){
                return response()->json(['message' => "Server exception",'status'=>false]);exit;
            }catch(ConnectException $e){
                return response()->json(['message' => "Connect exception",'status'=>false]);exit;
            }catch( TooManyRedirectsException $e){
                return response()->json(['message' => "TooMany redirects exception",'status'=>false]);exit;
            }catch(Exception $e){
                return response()->json(['message' => "Something wemt to wrong",'status'=>false]);exit;
            }catch(\Exception $e){
                return response()->json(['message' => "Something wemt to wrong",'status'=>false]);exit;
            }
        }else{
            return response()->json(['message' => "Site not exit",'status'=>false]);exit;
        }
    }
    public function store(Request $request)
    {
        // Validate fields with ajax
        $rules=array();
        $messages=array();
        $customAttributes=array();

        $rules["server_id"]="required";
        $rules["domain"]="required";
        $rules["project_type"]="required";
        $rules["username"]="required";

        $messages["server_id.required"]="Please select server";
        $messages["domain.required"]="Please enter domain name";
        $messages["project_type.required"]="Please select project type";
        $messages["username.required"]="Please enter user name";


        //$val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        /*if($validate->fails()){
            $validate->validate();
            //return response()->json(['errors' => $validate->messages()]);
            //var_dump($validate->messages());
        }*/

        $server = DB::table("user_apps")->where('id', $request->server_id)->where('user_id',auth()->user()->id)->first();

        if(!isset($server->id) || empty($server->server_id)){
            $rules["server_error"]="required";
            $messages["server_error.required"]="Server was not found";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            /*return redirect()
            ->route("voyager.{$dataType->slug}.create")
            ->with([
                //'message'    => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                'message'    => "server was not found",
                'alert-type' => 'error',
            ]);*/
        }
        $user_site_exits = DB::table("user_sites")->where('server_id', $request->server_id)->where('user_id',auth()->user()->id)->where('domain',$request->domain)->count();
        if($user_site_exits > 0){
            $rules["server_error"]="required";
            $messages["server_error.required"]=$request->doamin." has been already registered";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }
        $request->request->add(['user_id' => auth()->user()->id]);
        $request->request->add(['server_id' => $request->server_id]);
        $request->request->add(['site_id' => 2]);
        $request->request->add(['name' => "sdsdas"]);
        $request->request->add(['aliases' => ""]);
        $request->request->add(['directory' => ""]);
        $request->request->add(['wildcards' => ""]);
        $request->request->add(['site_status' => ""]);
        $request->request->add(['repository' => ""]);
        $request->request->add(['repository_provider' => ""]);
        $request->request->add(['repository_branch' => ""]);
        $request->request->add(['repository_status' => ""]);
        $request->request->add(['quick_deploy' => 0]);
        $request->request->add(['deployment_status' => ""]);
        $request->request->add(['project_type' => ""]);
        $request->request->add(['isolated' => 0]);
        $request->request->add(['app' => ""]);
        $request->request->add(['app_status' => ""]);
        $request->request->add(['hipchat_room' => ""]);
        $request->request->add(['slack_channel' => ""]);
        $request->request->add(['site_created_at' => ""]);
        $request->request->add(['username' => ""]);
        $request->request->add(['deployment_url' => ""]);
        UserSite::create($request->all());
        return redirect()
        ->route("wave.site")->with(['message' => 'Site created successfully', 'message_type' => 'success']);
        try{
            $client = new Client();
            $headers=[
                'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                //'Accept'=>'application/json',
                //'Content-Type'=>'application/json'
            ];
            $body=[
                'domain' => $request->domain,
                'project_type' => "php",
                //'username' => $request->username
            ];
            $res = $client->request('POST', 'https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites', [
                'form_params' => $body, 
                'headers' => $headers 
            ]);
            if ($res->getStatusCode() == 200) { // 200 OK
                //$response_data = $res->getBody()->getContents();
                //print_r($response_data);
                //exit;
                $response_data = $res->getBody()->getContents();
                $response_data=json_decode($response_data);
                //print_r($$response_data->site);exit;
                $request_data=array();
                if(isset($response_data->site->id)){
                    $request->request->add(['user_id' => auth()->user()->id]);
                    $request->request->add(['server_id' => $request->server_id]);
                    $request->request->add(['site_id' => $response_data->site->id]);
                    $request->request->add(['name' => $response_data->site->name]);
                    $request->request->add(['aliases' => json_encode($response_data->site->aliases)]);
                    $request->request->add(['directory' => $response_data->site->directory]);
                    $request->request->add(['wildcards' => $response_data->site->wildcards]);
                    $request->request->add(['site_status' => $response_data->site->status]);
                    $request->request->add(['repository' => $response_data->site->repository]);
                    $request->request->add(['repository_provider' => $response_data->site->repository_provider]);
                    $request->request->add(['repository_branch' => $response_data->site->repository_branch]);
                    $request->request->add(['repository_status' => $response_data->site->repository_status]);
                    $request->request->add(['quick_deploy' => 0]);
                    $request->request->add(['deployment_status' => $response_data->site->deployment_status]);
                    $request->request->add(['project_type' => $response_data->site->project_type]);
                    $request->request->add(['isolated' => 0]);
                    $request->request->add(['app' => $response_data->site->app]);
                    $request->request->add(['app_status' => $response_data->site->app_status]);
                    $request->request->add(['hipchat_room' => $response_data->site->hipchat_room]);
                    $request->request->add(['slack_channel' => $response_data->site->slack_channel]);
                    $request->request->add(['site_created_at' => $response_data->site->created_at]);
                    $request->request->add(['username' => $response_data->site->username]);
                    $request->request->add(['deployment_url' => $response_data->site->deployment_url]);
                }else{
                    $rules["server_error"]="required";
                    $messages["server_error.required"]="Something went to wrong with forge server because site detail is null";
                    $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
                }
                UserSite::create($request->all());
                return redirect()
                ->route("wave.site")->with(['message' => 'Site created successfully', 'message_type' => 'success']);
            }else{
                $val = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
            }
        }catch(ClientException $e){
            $responseBodyAsString = $e->getResponse()->getBody()->getContents();
            $rules["server_error"]="required";
            $messages["server_error.required"]="Client exception : ".$responseBodyAsString;
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }catch(RequestException $e){
            /*print_r($e->getResponse());
            if ($e->hasResponse()) 
            echo Psr7\str($e->getResponse());exit;*/
            $rules["server_error"]="required";
            $messages["server_error.required"]="Request exception";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }catch(BadResponseException $e){
            /*print_r($e->getResponse());
            if ($e->hasResponse()) 
            echo GuzzleHttp\Psr7\str($e->getResponse());exit;*/
            //print_r($e->getResponse());exit;
            $rules["server_error"]="required";
            $messages["server_error.required"]="Bad response exception";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }catch(ServerException $e){
            /*print_r($e->getResponse());
            if ($e->hasResponse()) 
            echo GuzzleHttp\Psr7\str($e->getResponse());exit;*/

            $rules["server_error"]="required";
            $messages["server_error.required"]="Server exception";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();

        }catch(ConnectException $e){
            /*print_r($e->getResponse());
            if ($e->hasResponse()) 
            echo GuzzleHttp\Psr7\str($e->getResponse());exit;*/

            $rules["server_error"]="required";
            $messages["server_error.required"]="Connect exception";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }catch( TooManyRedirectsException $e){
            /*print_r($e->getResponse());
            if ($e->hasResponse()) 
            echo $e->getResponse();exit;*/

            $rules["server_error"]="required";
            $messages["server_error.required"]="TooMany redirects exception";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();

        }catch(Exception $e){
            $rules["server_error"]="required";
            $messages["server_error.required"]="Something wemt to wrong";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }catch(\Exception $e){
            $rules["server_error"]="required";
            $messages["server_error.required"]="Something wemt to wrong";
            $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        }
    }

    public function restore(Request $request, $id)
    {

    }

    public function deployView(Request $request,$id)
    {

    }

    
    public function deploy(Request $request,$id)
    {


    }

    public function deployScriptView(Request $request,$id)
    {
        $site=UserSite::where("id",$id)->where("user_id",auth()->user()->id)->first();
        if(!isset($site->id)){
            return response()->json(['message' => "Site was not found","status"=>false]);exit;    
        }
        return response()->json(['message' => "Get data successfully","status"=>true,"deployment_script"=>$site->deployment_script]);exit;
    }

    public function deployScript(Request $request,$id)
    {
        $site=UserSite::where("id",$id)->where("user_id",auth()->user()->id)->first();
        if(!isset($site->id)){
            return response()->json(['message' => "Site was not found","status"=>false]);exit;    
        }

        // Validate fields with ajax
        $rules=array();
        $messages=array();
        $customAttributes=array();
        
        $rules["deployment_script"]="required";
        $messages["deployment_script.required"]="Please enter deploy script";

        $validate = Validator::make($request->all(), $rules, $messages, $customAttributes);
        foreach ($validate->errors()->all() as $error) {
            return response()->json(['message' => $error,"status"=>false]);exit;   
        }
        $server = DB::table("user_apps")->where('id', $site->server_id)->first();
        if(!isset($server->id) || empty($server->server_id)){
            return response()->json(['message' => "Site was not found","status"=>false]);exit;   
        }
        if($site->site_status != "installed"){
            return response()->json(['message' => "Site is not installed","status"=>false]);exit;
        }else{
            //Temp Test
            /*$site->deployment_script=$request->deployment_script;
            if($site->save()){
                return response()->json(['message' => "Deploy script updated successfully","status"=>true]);exit;
            }else{
                return response()->json(['message' => "Failed to update deploy script","status"=>false]);exit;
            }*/

            try{
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                    //'Accept'=>'application/json',
                    //'Content-Type'=>'application/json'
                ];
                $body=[
                    'content'=>$request->deployment_script,
                ];
                $res = $client->request('PUT','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$site->site_id.'/deployment/script', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);
                if ($res->getStatusCode() == 200) { // 200 OK
                    $site->deployment_script=$request->deployment_script;
                    if($site->save()){
                        return response()->json(['message' => "Deploy script updated successfully","status"=>true]);exit;
                    }else{
                        return response()->json(['message' => "Failed to update deploy script","status"=>false]);exit;
                    }
                }else{
                    return response()->json(['message' => "Can not update script to server","status"=>false]);exit;
                }
            }catch(ClientException $e){
                $responseBodyAsString = $e->getResponse()->getBody()->getContents();
                return response()->json(['message' => "Client exception : ".$responseBodyAsString,"status"=>false]);exit;
            }catch(RequestException $e){
                return response()->json(['message' => "Request exception","status"=>false]);exit;
            }catch(BadResponseException $e){
                return response()->json(['message' => "Bad response exception","status"=>false]);exit;
            }catch(ServerException $e){
                return response()->json(['message' => "Server exception","status"=>false]);exit;
            }catch(ConnectException $e){
                return response()->json(['message' => "Connect exception","status"=>false]);exit;
            }catch( TooManyRedirectsException $e){
                return response()->json(['message' => "TooMany redirects exception","status"=>false]);exit;
            }catch(Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }catch(\Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }
        }

    }

    public function deployNow(Request $request,$id)
    {

        $site=UserSite::where("id",$id)->where("user_id",auth()->user()->id)->first();
        if(!isset($site->id)){
            return response()->json(['message' => "Site was not found","status"=>false]);exit;    
        }
        $server = DB::table("user_apps")->where('id', $site->server_id)->first();
        if(!isset($server->id) || empty($server->server_id)){
            return response()->json(['message' => "Site was not found","status"=>false]);exit;   
        }
        if($site->site_status != "installed"){
            return response()->json(['message' => "Site is not installed","status"=>false]);exit;
        }else{
            try{
                //Temp Test
                //return response()->json(['message' => "Deploy in progress successfully","status"=>true]);exit;
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                    //'Accept'=>'application/json',
                    //'Content-Type'=>'application/json'
                ];
                $body=[];
                $res = $client->request('POST','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$dataTypeContent->site_id.'/deployment/deploy', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);
                if ($res->getStatusCode() == 200) { // 200 OK
                    return response()->json(['message' => "Deploy in progress successfully","status"=>true]);exit;
                }else{
                    return response()->json(['message' => "Failed to Deploy Now","status"=>false]);exit;
                }
                
            }catch(ClientException $e){
                $responseBodyAsString = $e->getResponse()->getBody()->getContents();
                return response()->json(['message' => "Client exception : ".$responseBodyAsString,"status"=>false]);exit;
            }catch(RequestException $e){
                return response()->json(['message' => "Request exception","status"=>false]);exit;
            }catch(BadResponseException $e){
                return response()->json(['message' => "Bad response exception","status"=>false]);exit;
            }catch(ServerException $e){
                return response()->json(['message' => "Server exception","status"=>false]);exit;
            }catch(ConnectException $e){
                return response()->json(['message' => "Connect exception","status"=>false]);exit;
            }catch( TooManyRedirectsException $e){
                return response()->json(['message' => "TooMany redirects exception","status"=>false]);exit;
            }catch(Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }catch(\Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }
        }
    }
    
    public function deployScriptFromServer(Request $request,$id)
    {


    }

    public function envView(Request $request,$id)
    {
        $site=UserSite::where("id",$id)->where("user_id",auth()->user()->id)->first();
        if(!isset($site->id)){
            return response()->json(['message' => "Site was not found","status"=>false]);exit;    
        }
        $server = DB::table("user_apps")->where('id', $site->server_id)->first();
        if(!isset($server->id) || empty($server->server_id)){
            return response()->json(['message' => "server was not found","status"=>false]);exit;   
        }
        if($site->site_status != "installed"){
            return response()->json(['message' => "Site is not installed","status"=>false]);exit;
        }else if(empty($site->env_file)){
            try{
                //Temp Test
                //return response()->json(['message' => "Get data successfully","status"=>true,"env_file"=>$site->env_file]);exit;

                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                    //'Accept'=>'application/json',
                    //'Content-Type'=>'application/json'
                ];
                $body=[];
                $res = $client->request('GET','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$site->site_id.'/env', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);
                if ($res->getStatusCode() == 200) { // 200 OK
                    $response_data = $res->getBody()->getContents();
                    if(!empty($response_data)){
                        $site->env_file==trim($response_data);
                        $site->save();
                        return response()->json(['message' => "Get data successfully","status"=>true,"env_file"=>$site->env_file]);exit;
                    }
                }
                return response()->json(['message' => "Can not fetch detail","status"=>false]);exit;
            }catch(ClientException $e){
                $responseBodyAsString = $e->getResponse()->getBody()->getContents();
                return response()->json(['message' => "Client exception : ".$responseBodyAsString,"status"=>false]);exit;
            }catch(RequestException $e){
                return response()->json(['message' => "Request exception","status"=>false]);exit;
            }catch(BadResponseException $e){
                return response()->json(['message' => "Bad response exception","status"=>false]);exit;
            }catch(ServerException $e){
                return response()->json(['message' => "Server exception","status"=>false]);exit;
            }catch(ConnectException $e){
                return response()->json(['message' => "Connect exception","status"=>false]);exit;
            }catch( TooManyRedirectsException $e){
                return response()->json(['message' => "TooMany redirects exception","status"=>false]);exit;
            }catch(Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }catch(\Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }
        }
        return response()->json(['message' => "Get data successfully","status"=>true,"env_file"=>$site->env_file]);exit;
    }

    public function envUpdate(Request $request,$id)
    {
        $site=UserSite::where("id",$id)->where("user_id",auth()->user()->id)->first();
        if(!isset($site->id)){
            return response()->json(['message' => "Site was not found","status"=>false]);exit;    
        }
        $server = DB::table("user_apps")->where('id', $site->server_id)->first();
        if(!isset($server->id) || empty($server->server_id)){
            return response()->json(['message' => "server was not found","status"=>false]);exit;   
        }
        if(!$request->has("env_file") || empty($request->get("env_file"))){
            return response()->json(['message' => "Env can not be empty(var)","status"=>false]);exit;      
        }
        if($site->site_status != "installed"){
            return response()->json(['message' => "Site is not installed","status"=>false]);exit;
        }else{
            try{
                //Temp Test
                //return response()->json(['message' => "Env file updated successfully","status"=>true,"env_file"=>$site->env_file]);exit;
                
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                    //'Accept'=>'application/json',
                    //'Content-Type'=>'application/json'
                ];
                $body=[
                    'content'=>$request->env_file,
                ];
                $res = $client->request('PUT','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$site->site_id.'/env', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);
                if ($res->getStatusCode() == 200) { // 200 OK
                    $site->env_file=$request->get("env_file");
                    $site->save();
                    return response()->json(['message' => "Env file updated successfully","status"=>true]);exit;
                }
                return response()->json(['message' => "Can not update env file","status"=>false]);exit;
            }catch(ClientException $e){
                $responseBodyAsString = $e->getResponse()->getBody()->getContents();
                return response()->json(['message' => "Client exception : ".$responseBodyAsString,"status"=>false]);exit;
            }catch(RequestException $e){
                return response()->json(['message' => "Request exception","status"=>false]);exit;
            }catch(BadResponseException $e){
                return response()->json(['message' => "Bad response exception","status"=>false]);exit;
            }catch(ServerException $e){
                return response()->json(['message' => "Server exception","status"=>false]);exit;
            }catch(ConnectException $e){
                return response()->json(['message' => "Connect exception","status"=>false]);exit;
            }catch( TooManyRedirectsException $e){
                return response()->json(['message' => "TooMany redirects exception","status"=>false]);exit;
            }catch(Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }catch(\Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }
        }
        return response()->json(['message' => "Get data successfully","status"=>true,"env_file"=>$site->env_file]);exit;
    }

    public function createAjax(Request $request)
    {
        //Adding User Id For This Insertion
        $request->request->add(['user_id' =>auth()->user()->id]);

        // Validate fields with ajax
        $rules=array();
        $messages=array();
        $customAttributes=array();
        
        $rules["server_id"]="required";
        $rules["domain"]="required";
        $rules["project_type"]="required";
        $rules["username"]="required";

        $messages["server_id.required"]="Please select server";
        $messages["domain.required"]="Please enter domain name";
        $messages["project_type.required"]="Please select project type";
        $messages["username.required"]="Please enter user name";

        
        //$val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        //$validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
        $validate = Validator::make($request->all(), $rules, $messages, $customAttributes);
        if($validate->fails()){
            //$validate->validate();
            //return response()->json(['errors' => $validate->messages()]);
            //var_dump($validate->messages());
            foreach ($validate->errors()->all() as $error) {
                return response()->json(['message' => $error,'status'=>false]);exit;
            }
        }

        //$server = DB::table("user_apps")->where('id', $request->server_id)->where('user_id',auth()->user()->id)->first();
        $server = DB::table("user_apps")->where('id', $request->server_id)->first();

        if(!isset($server->id) || empty($server->server_id)){
            return response()->json(['message' => "Server was not found",'status'=>false]);exit;
        }else if($server->is_ready != 1){
            return response()->json(['message' => "Server is not ready yet",'status'=>false]);exit;
        }
        $user_site_exits = DB::table("user_sites")->where('server_id', $request->server_id)->where('user_id',auth()->user()->id)->where('domain',$request->domain)->count();
        if($user_site_exits > 0){
            return response()->json(['message' => $request->doamin." has been already registered",'status'=>false]);exit;
        }

        /*Temp Test*/
        /*$request->request->add(['user_id' => auth()->user()->id]);
        $request->request->add(['server_id' => $request->server_id]);
        $request->request->add(['site_id' => 2]);
        $request->request->add(['name' => "sdsdas"]);
        $request->request->add(['aliases' => ""]);
        $request->request->add(['directory' => ""]);
        $request->request->add(['wildcards' => 0]);
        $request->request->add(['site_status' => ""]);
        $request->request->add(['repository' => ""]);
        $request->request->add(['repository_provider' => ""]);
        $request->request->add(['repository_branch' => ""]);
        $request->request->add(['repository_status' => ""]);
        $request->request->add(['quick_deploy' => 0]);
        $request->request->add(['deployment_status' => ""]);
        $request->request->add(['project_type' => ""]);
        $request->request->add(['isolated' => 0]);
        $request->request->add(['app' => ""]);
        $request->request->add(['app_status' => ""]);
        $request->request->add(['hipchat_room' => ""]);
        $request->request->add(['slack_channel' => ""]);
        //$request->request->add(['site_created_at' => ""]);
        $request->request->add(['username' => ""]);
        $request->request->add(['deployment_url' => ""]);

        $site=UserSite::create($request->all());
        return response()->json(['message' => "Site created successfully",'status'=>true,'id'=>$site->id]);exit;*/

        try{
            //remove
            //return response()->json(['message' => "status",'status'=>true,'id'=>16]);exit;
            $client = new Client();
            $headers=[
                'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                //'Accept'=>'application/json',
                //'Content-Type'=>'application/json'
            ];
            $body=[
                'domain' => $request->domain,
                'project_type' => "php",
                'username' => $request->username
            ];
            $res = $client->request('POST', 'https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites', [
                'form_params' => $body, 
                'headers' => $headers 
            ]);
            if ($res->getStatusCode() == 200) { // 200 OK
                //$response_data = $res->getBody()->getContents();
                //print_r($response_data);
                //exit;
                $response_data = $res->getBody()->getContents();
                $response_data=json_decode($response_data);
                //print_r($$response_data->site);exit;
                $request_data=array();
                if(isset($response_data->site->id)){
                    $request->request->add(['user_id' => auth()->user()->id]);
                    $request->request->add(['server_id' => $request->server_id]);
                    $request->request->add(['site_id' => $response_data->site->id]);
                    $request->request->add(['name' => $response_data->site->name]);
                    $request->request->add(['aliases' => json_encode($response_data->site->aliases)]);
                    $request->request->add(['directory' => $response_data->site->directory]);
                    $request->request->add(['wildcards' => $response_data->site->wildcards]);
                    $request->request->add(['site_status' => $response_data->site->status]);
                    $request->request->add(['repository' => $response_data->site->repository]);
                    $request->request->add(['repository_provider' => $response_data->site->repository_provider]);
                    $request->request->add(['repository_branch' => $response_data->site->repository_branch]);
                    $request->request->add(['repository_status' => $response_data->site->repository_status]);
                    $request->request->add(['quick_deploy' => 0]);
                    $request->request->add(['deployment_status' => $response_data->site->deployment_status]);
                    $request->request->add(['project_type' => $response_data->site->project_type]);
                    $request->request->add(['isolated' => 0]);
                    $request->request->add(['app' => $response_data->site->app]);
                    $request->request->add(['app_status' => $response_data->site->app_status]);
                    //$request->request->add(['hipchat_room' => $response_data->site->hipchat_room]);
                    $request->request->add(['slack_channel' => $response_data->site->slack_channel]);
                    $request->request->add(['site_created_at' => $response_data->site->created_at]);
                    $request->request->add(['username' => $response_data->site->username]);
                    $request->request->add(['deployment_url' => $response_data->site->deployment_url]);
                }else{
                    return response()->json(['message' => "Something went to wrong with forge server because site detail is null",'status'=>false]);exit;
                }
                UserSite::create($request->all());
                return response()->json(['message' => "Site created successfully",'status'=>true]);exit;
            }else{
                return response()->json(['message' => "Something went to wrong with forge server because site detail is null",'status'=>false]);exit;
            }
        }catch(ClientException $e){
            $responseBodyAsString = $e->getResponse()->getBody()->getContents();
            return response()->json(['message' => "Client exception : ".$responseBodyAsString,'status'=>false]);exit;
        }catch(RequestException $e){
            return response()->json(['message' =>"Request exception",'status'=>false]);exit;
        }catch(BadResponseException $e){
            return response()->json(['message' =>"Bad response exception",'status'=>false]);exit;
        }catch(ServerException $e){
            return response()->json(['message' =>"Server exception",'status'=>false]);exit;
        }catch(ConnectException $e){
            return response()->json(['message' =>"Connect exception",'status'=>false]);exit;
        }catch( TooManyRedirectsException $e){
            return response()->json(['message' =>"TooMany redirects exception",'status'=>false]);exit;
        }catch(Exception $e){
            return response()->json(['message' =>"Something wemt to wrong",'status'=>false]);exit;
        }catch(\Exception $e){
            return response()->json(['message' =>"Something wemt to wrong",'status'=>false]);exit;
        }

    }

    public function repoAjax(Request $request){
        if(!$request->has("id")){
            return response()->json(['message' => "Site was not found",'status'=>false]);exit;
        }
        $id=$request->get("id");
        // Validate fields with ajax
        $rules=array();
        $messages=array();
        $customAttributes=array();
        
        $rules["repository"]="required";
        $rules["repository_provider"]="required";
        $rules["branch"]="required";

        $messages["repository.required"]="Please enter repository";
        $messages["repository_provider.required"]="Please select repository provider";
        $messages["branch.required"]="Please enter branch";

        //$val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $validate = Validator::make($request->all(), $rules, $messages, $customAttributes);
        if($validate->fails()){
            foreach ($validate->errors()->all() as $error) {
                return response()->json(['message' => $error,'status'=>false]);exit;
            }
        }
        $site = DB::table("user_sites")->where('user_id',auth()->user()->id)->where('id', $id)->first();
        if(!isset($site->id)){
            return response()->json(['message' => "Site was not found","status"=>false]);exit;
        }
        $server = DB::table("user_apps")->where('id', $site->server_id)->first();
        if(!isset($server->id) || empty($server->server_id)){
            return response()->json(['message' => "Server was not found","status"=>false]);exit;
        }

        /*Temp Test*/
        /*DB::table('user_sites')->where('id', $site->id)->update(['repository' => $request->repository,'repository_provider'=>$request->repository_provider,'repository_branch'=>$request->branch]);
        return response()->json(['message' => "installed successfully","status"=>true]);exit;*/


        if($site->site_status != "installed"){
            try{
                //remove
                //return response()->json(['message' => "installed successfully","status"=>true]);exit;    
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                        //'Accept'=>'application/json',
                        //'Content-Type'=>'application/json'
                ];
                $body=[];
                $res = $client->request('GET', 'https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);
                if ($res->getStatusCode() == 200) { // 200 OK
                    $response_data = $res->getBody()->getContents();
                    $response_data=json_decode($response_data);
                    $request_data=array();

                    if(isset($response_data->sites) && count($response_data->sites) > 0){
                        $found=false;
                        foreach ($response_data->sites as $site_detail) {
                            //echo $site->id."-".$site->status;
                            if($site_detail->id == $site->site_id){
                                if($site->status == "installed"){
                                    $found=true;
                                }
                                DB::table('user_sites')->where('id', $site->id)->update(['site_status' => $site_detail->status]);
                            }

                        }
                        if(!$found){
                            return response()->json(['message' => "Site was not installed","status"=>false]);exit;    
                        }else{
                            try{
                                $client = new Client();
                                $headers=[
                                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                                    //'Accept'=>'application/json',
                                    //'Content-Type'=>'application/json'
                                ];
                                $body=[
                                    'repository'=>$request->repository,
                                    'provider'=>$request->repository_provider,
                                    'branch'=>$request->branch
                                ];
                                $res = $client->request('POST','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$site->site_id.'/git', [
                                    'form_params' => $body, 
                                    'headers' => $headers 
                                ]);
                                //print_r($res);exit;
                                //echo $res->getStatusCode();exit;
                                if ($res->getStatusCode() == 200) { // 200 OK
                                        //$response_data = $res->getBody()->getContents();
                                        //$response_data=json_decode($response_data);
                                        //print_r($res->getBody());exit;
                                    //$this->insertUpdateData($request, $slug, $dataType->editRows, $data);
                                    DB::table('user_sites')->where('id', $site->id)->update(['repository' => $request->repository,'repository_provider'=>$request->repository_provider,'repository_branch'=>$request->branch]);
                                    return response()->json(['message' => "Deployment saved success and please wait untill it finished","status"=>true]);exit;
                                }
                            }catch(ClientException $e){
                                return response()->json(['message' => "Client exception : ".$responseBodyAsString,"status"=>false]);exit;
                            }catch(RequestException $e){
                                return response()->json(['message' => "Request exception","status"=>false]);exit;
                            }catch(BadResponseException $e){
                                return response()->json(['message' => "Bad response exception","status"=>false]);exit;
                            }catch(ServerException $e){
                                return response()->json(['message' => "Server exception","status"=>false]);exit;
                            }catch(ConnectException $e){
                                return response()->json(['message' => "Connect exception","status"=>false]);exit;
                            }catch( TooManyRedirectsException $e){
                                return response()->json(['message' => "TooMany redirects exception","status"=>false]);exit;
                            }catch(Exception $e){
                                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
                            }catch(\Exception $e){
                                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
                            }
                        }
                    }else{
                        return response()->json(['message' => "Site was not found on server.","status"=>false]);exit;
                    }
                    //$data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
                    //return response()->json(['message' => "Deployment saved success and please wait untill it finished","status"=>true]);exit;
                }else{
                    return response()->json(['message' => "Can not fetch detail from server.","status"=>false]);exit;
                }
            }catch(ClientException $e){
                return response()->json(['message' => "Client exception : ".$responseBodyAsString,"status"=>false]);exit;
            }catch(RequestException $e){
                return response()->json(['message' => "Request exception","status"=>false]);exit;
            }catch(BadResponseException $e){
                return response()->json(['message' => "Bad response exception","status"=>false]);exit;
            }catch(ServerException $e){
                return response()->json(['message' => "Server exception","status"=>false]);exit;
            }catch(ConnectException $e){
                return response()->json(['message' => "Connect exception","status"=>false]);exit;
            }catch( TooManyRedirectsException $e){
                return response()->json(['message' => "TooMany redirects exception","status"=>false]);exit;
            }catch(Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }catch(\Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }
            return response()->json(['message' => "Site is not installed","status"=>false]);exit;
        }else{
            try{
                return response()->json(['message' => "installed successfully","status"=>true]);exit;
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                    //'Accept'=>'application/json',
                    //'Content-Type'=>'application/json'
                ];
                $body=[
                    'repository'=>$request->repository,
                    'provider'=>$request->repository_provider,
                    'branch'=>$request->branch
                ];
                $res = $client->request('POST','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$site->site_id.'/git', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);

                if ($res->getStatusCode() == 200) { // 200 OK
                    //$this->insertUpdateData($request, $slug, $dataType->editRows, $data);
                    DB::table('user_sites')->where('id', $site->id)->update(['repository' => $request->repository,'repository_provider'=>$request->repository_provider,'repository_branch'=>$request->branch]);
                    return response()->json(['message' => "Deployment saved success and please wait untill it finished","status"=>true]);exit;
                }
            }catch(ClientException $e){
                return response()->json(['message' => "Client exception : ".$responseBodyAsString,"status"=>false]);exit;
            }catch(RequestException $e){
                return response()->json(['message' => "Request exception","status"=>false]);exit;
            }catch(BadResponseException $e){
                return response()->json(['message' => "Bad response exception","status"=>false]);exit;
            }catch(ServerException $e){
                return response()->json(['message' => "Server exception","status"=>false]);exit;
            }catch(ConnectException $e){
                return response()->json(['message' => "Connect exception","status"=>false]);exit;
            }catch( TooManyRedirectsException $e){
                return response()->json(['message' => "TooMany redirects exception","status"=>false]);exit;
            }catch(Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }catch(\Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }
        }
    }
    public function getDeployScriptAjax(Request $request)
    {
        if(!$request->has("id")){
            return response()->json(['message' => "Site was not found",'status'=>false]);exit;
        }
        $id=$request->get("id");
        $customAttributes=array();
        $viewType="deployScript";

        //Checking if the user belongs to the data
        $site = DB::table("user_sites")->where('id', $id)->where('user_id',auth()->user()->id)->first();
        if(!isset($site->id)){
            return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
        }

        /*Temp Test*/
        /*$site->deployment_script="asdsadasdsadsadsa";
        return response()->json(['message' => "get deploy script successfully","status"=>true,"data"=>$site->deployment_script]);exit;*/

        /*Get script from server*/
        if(empty($site->deployment_script)){
            $server = DB::table("user_apps")->where('id', $site->server_id)->where('user_id',auth()->user()->id)->first();
            if(!isset($server->id) || empty($server->server_id)){
                return response()->json(['message' => "Server was not found","status"=>false]);exit;
            }
            if($site->site_status != "installed"){
                return response()->json(['message' => "Site is not installed","status"=>false]);exit;
            }
            try{
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                    //'Accept'=>'application/json',
                    //'Content-Type'=>'application/json'
                ];
                $body=[];
                $res = $client->request('GET','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$site->site_id.'/deployment/script', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);
                if ($res->getStatusCode() == 200) { // 200 OK
                    $response_data = $res->getBody()->getContents();
                    if(!empty($response_data)){
                        $site->deployment_script=trim($response_data);
                        return response()->json(['message' => "get deploy script successfully","status"=>true,"data"=>$site->deployment_script]);exit;   
                    }
                }else{
                    return response()->json(['message' => "Failed to get default script from server","status"=>false]);exit;
                }
                
            }catch(ClientException $e){
                return response()->json(['message' => "Client exception : ".$responseBodyAsString,"status"=>false]);exit;
            }catch(RequestException $e){
                return response()->json(['message' => "Request exception","status"=>false]);exit;
            }catch(BadResponseException $e){
                return response()->json(['message' => "Bad response exception","status"=>false]);exit;
            }catch(ServerException $e){
                return response()->json(['message' => "Server exception","status"=>false]);exit;
            }catch(ConnectException $e){
                return response()->json(['message' => "Connect exception","status"=>false]);exit;
            }catch( TooManyRedirectsException $e){
                return response()->json(['message' => "TooMany redirects exception","status"=>false]);exit;
            }catch(Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }catch(\Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }
        }else{
            return response()->json(['message' => "get deploy script successfully","status"=>true,"data"=>$site->deployment_script]);exit;
        }
    }
    public function deployScriptAjax(Request $request)
    {
        if(!$request->has("id")){
            return response()->json(['message' => "Site was not found",'status'=>false]);exit;
        }
        $id=$request->get("id");

        // Validate fields with ajax
        $rules=array();
        $messages=array();
        $customAttributes=array();
        
        $rules["deployment_script"]="required";
        $messages["deployment_script.required"]="Please enter deploy script";

        $validate = Validator::make($request->all(), $rules, $messages, $customAttributes);
        if($validate->fails()){
            foreach ($validate->errors()->all() as $error) {
                return response()->json(['message' => $error,'status'=>false]);exit;
            }
        }
        $site = DB::table("user_sites")->where('id', $id)->where('user_id',auth()->user()->id)->first();
        $server = DB::table("user_apps")->where('id', $site->server_id)->first();
        if(!isset($server->id) || empty($server->server_id)){
            return response()->json(['message' => "Server was not found",'status'=>false]);exit;
        }
        /*Temp Test*/
        /*DB::table('user_sites')->where('id', $site->id)->update(['deployment_script' => $request->deployment_script]);
        return response()->json(['message' => "Repo install successfully","status"=>true]);exit;*/

        if($site->site_status != "installed"){
            return response()->json(['message' => "Site is not installed",'status'=>false]);exit;
        }else{
            try{
                return response()->json(['message' => "done",'status'=>true]);exit;  
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                    //'Accept'=>'application/json',
                    //'Content-Type'=>'application/json'
                ];
                $body=[
                    'content'=>$request->deployment_script,
                ];
                $res = $client->request('PUT','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$site->site_id.'/deployment/script', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);
                if ($res->getStatusCode() == 200) { // 200 OK
                    DB::table('user_sites')->where('id', $site->id)->update(['deployment_script' => $request->deployment_script]);
                    return response()->json(['message' => "Repo install successfully","status"=>true]);exit;
                }else{
                    return response()->json(['message' => "Can not update script to server","status"=>false]);exit;
                }
                
            }catch(ClientException $e){
                $responseBodyAsString = $e->getResponse()->getBody()->getContents();
                return response()->json(['message' => "Client exception : ".$responseBodyAsString,"status"=>false]);exit;
            }catch(RequestException $e){
                return response()->json(['message' => "Request exception","status"=>false]);exit;
            }catch(BadResponseException $e){
                return response()->json(['message' => "Bad response exception","status"=>false]);exit;
            }catch(ServerException $e){
                return response()->json(['message' => "Server exception","status"=>false]);exit;
            }catch(ConnectException $e){
                return response()->json(['message' => "Connect exception","status"=>false]);exit;
            }catch( TooManyRedirectsException $e){
                return response()->json(['message' => "TooMany redirects exception","status"=>false]);exit;
            }catch(Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }catch(\Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }
        }
    }
    public function getEnvAjax(Request $request)
    {
        if(!$request->has("id")){
            return response()->json(['message' => "Site was not found",'status'=>false]);exit;
        }
        $id=$request->get("id");
        $customAttributes=array();
        $viewType="env";

        $site=DB::table("user_sites")->where('id', $id)->where('user_id',auth()->user()->id)->first();
        if(!$site->id){
            return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
        }

        /*Temp Test*/
        /*$site->env_file=trim("asdsadsadsds");
        return response()->json(['message' => "Get default env successfully","status"=>true,"data"=>$site->env_file]);exit;*/

        /*Get script from server*/
        if(empty($site->env_file)){
            $server = DB::table("user_apps")->where('id', $site->server_id)->where('user_id',auth()->user()->id)->first();
            if(!isset($server->id) || empty($server->server_id)){
                return response()->json(['message' => "Server was not found","status"=>false]);exit;
            }
            if($site->site_status != "installed"){
                return response()->json(['message' => "Site is not installed","status"=>false]);exit;
            }
            try{
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                    //'Accept'=>'application/json',
                    //'Content-Type'=>'application/json'
                ];
                $body=[];
                $res = $client->request('GET','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$site->site_id.'/env', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);
                if ($res->getStatusCode() == 200) { // 200 OK
                    $response_data = $res->getBody()->getContents();
                    if(!empty($response_data)){
                        $site->env_file=trim($response_data);
                        return response()->json(['message' => "Get default env successfully","status"=>true,"data"=>$site->env_file]);exit;
                    }
                }
                return response()->json(['message' => "Failed to get default env","status"=>false]);exit;
                
            }catch(ClientException $e){
                $responseBodyAsString = $e->getResponse()->getBody()->getContents();
                return response()->json(['message' => "Client exception : ".$responseBodyAsString,"status"=>false]);exit;
            }catch(RequestException $e){
                $rules["server_error"]="required";
                $messages["server_error.required"]="Request exception";
                $validate = Validator::make($request->all(), $rules, $messages, $customAttributes)->validate();
                return response()->json(['message' => "Request exception","status"=>false]);exit;
            }catch(BadResponseException $e){
                return response()->json(['message' => "Bad response exception","status"=>false]);exit;
            }catch(ServerException $e){
                return response()->json(['message' => "Server exception","status"=>false]);exit;
            }catch(ConnectException $e){
                return response()->json(['message' => "Connect exception","status"=>false]);exit;
            }catch( TooManyRedirectsException $e){
                return response()->json(['message' => "TooMany redirects exception","status"=>false]);exit;
            }catch(Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }catch(\Exception $e){
                return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
            }
        }
        return response()->json(['message' => "Get default env successfully","status"=>true,"data"=>$site->env_file]);exit;
    }

    public function updateEnvAjax(Request $request)
    {
        if(!$request->has("id")){
            return response()->json(['message' => "Site was not found",'status'=>false]);exit;
        }
        $id=$request->get("id");
        $site=DB::table("user_sites")->where('id', $id)->where('user_id',auth()->user()->id)->first();
        if(!$site->id){
            return response()->json(['message' => "Something wemt to wrong","status"=>false]);exit;
        }

        // Validate fields with ajax
        $rules=array();
        $messages=array();
        $customAttributes=array();
        
        $rules["env_file"]="required";
        $messages["env_file.required"]="Please enter env script";

        $validate = Validator::make($request->all(), $rules, $messages, $customAttributes);
        if($validate->fails()){
            foreach ($validate->errors()->all() as $error) {
                return response()->json(['message' => $error,'status'=>false]);exit;
            }
        }
        $server = DB::table("user_apps")->where('id', $site->server_id)->first();
        if(!isset($server->id) || empty($server->server_id)){
            return response()->json(['message' => "Server was not found",'status'=>false]);exit;
        }

        /*Temp Test*/
        /*DB::table('user_sites')->where('id', $site->id)->update(['env_file' => $request->env_file]);
        return response()->json(['message' => "Env file updated successfully.",'status'=>true]);exit;*/


        if($site->site_status != "installed"){
            return response()->json(['message' => "Site is not installed",'status'=>false]);exit;
        }else{
            try{
                //remove
                //return response()->json(['message' => "Env file updated successfully.",'status'=>true]);exit;
                $client = new Client();
                $headers=[
                    'Authorization'=>'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEzYjU1NjE1MjNmMWZhOTgwMTVmNjg3YzhiYmRmZWZlMGNiZGQxNjQxM2Q1OWExMTQyNjYyMDZjY2MwYWZkNzBmNzYzNWVkZDFiYjgyZWNkIn0.eyJhdWQiOiIxIiwianRpIjoiYTNiNTU2MTUyM2YxZmE5ODAxNWY2ODdjOGJiZGZlZmUwY2JkZDE2NDEzZDU5YTExNDI2NjIwNmNjYzBhZmQ3MGY3NjM1ZWRkMWJiODJlY2QiLCJpYXQiOjE1Nzc4ODc3MjEsIm5iZiI6MTU3Nzg4NzcyMSwiZXhwIjoxODkzNTA2OTIxLCJzdWIiOiIxMTQxNzEiLCJzY29wZXMiOltdfQ.Q7l-aZsb0xtHa1unvXCCaiLLEi86dtYWpxWHz76WU6wHGTD5-FWhgbEgI7HST_gSenar8-Zi33SttEpMsgC5pOlaR2ODiNGR0aodjWhwN2BHuLHHTjM_xVB6_sQ0ZDtei7jKt6zKXo2BcaFXjG2x_yiR4MN-4c6RCpTG5zDWTy5PdehcNXgEJD9iSKc7sVx6gyH1GSBjYGbE71gyXyjw1-4cD0sZQON2IJ8XDaVNEQ82LhD4-HmWZFd59Kujv6dMb8OrjXmzHAZxwrHDT2um9JvS3s_sWyodsOhoQEw_SPBUEu7nNkAuEmWvwKC4A7vqJK-sRVlTk_S5Z3HIT_iLf__cuQ7hDdylidsGnDYNbFvDmsd31X076H9tl7gi0tQPAFusmlrgqRXPf5H1GG840VEzmafsdEnUc5nG6zGnxhm4_kkMfjRLX_rpKcrS3oiyK1JhJc9y8UlyM-ZPOo2Lj6pZTHQhQB18_khozm3Ex_710KO33j2SGGY1HSpZsWYSCsn91bavg7dk4nB0Ytqb4wfGJGAAGoVNHGslxKZISAbPLbUkpBkPs1YnKXGarQs05EcI95uWd2XVPCwGOXDVQ8zPZb-WIagAJSkJHXoK8vD8hwCqmWvkNgoqXH9Iif47_1PtNIPZlXhCi0QwlmjqHpQGvEH02xhUGqymWeKMmeo',
                    //'Accept'=>'application/json',
                    //'Content-Type'=>'application/json'
                ];
                $body=[
                    'content'=>$request->env_file,
                ];
                $res = $client->request('PUT','https://forge.laravel.com/api/v1/servers/'.$server->server_id.'/sites/'.$site->site_id.'/env', [
                    'form_params' => $body, 
                    'headers' => $headers 
                ]);
                if ($res->getStatusCode() == 200) { // 200 OK
                    DB::table('user_sites')->where('id', $site->id)->update(['env_file' => $request->env_file]);
                    return response()->json(['message' => "Env file updated successfully.",'status'=>true]);exit;
                }else{
                    return response()->json(['message' => "Can not update env file to server",'status'=>false]);exit;
                }
                
            }catch(ClientException $e){
                return response()->json(['message' => "Client exception : ".$responseBodyAsString,'status'=>false]);exit;
            }catch(RequestException $e){
                return response()->json(['message' => "Request exception",'status'=>false]);exit;
            }catch(BadResponseException $e){
                return response()->json(['message' => "Bad response exception",'status'=>false]);exit;
            }catch(ServerException $e){
                return response()->json(['message' => "Server exception",'status'=>false]);exit;
            }catch(ConnectException $e){
                return response()->json(['message' => "Connect exception",'status'=>false]);exit;
            }catch( TooManyRedirectsException $e){
                return response()->json(['message' => "TooMany redirects exception",'status'=>false]);exit;
            }catch(Exception $e){
                return response()->json(['message' => "Something wemt to wrong",'status'=>false]);exit;
            }catch(\Exception $e){
                return response()->json(['message' => "Something wemt to wrong",'status'=>false]);exit;
            }
        }
    }
}
