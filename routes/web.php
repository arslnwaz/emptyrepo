<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});*/

// Authentication routes
Auth::routes();

Route::group(['prefix' => 'admin'], function () {

    Voyager::routes();

    /*Customize Route*/

    //Route::get('/user-sites', ['uses' => 'Voyager\UserSiteController@index', 'as' => 'voyager.user-sites.index']);

    //Site Deploy repo
   	Route::get('user-sites-deploy/{id}', ['uses' => 'Voyager\UserSiteController@deployView', 'as' => 'VoyegarUserSiteDeploy']);
   	Route::put('user-sites-deploy/{id}', ['uses' => 'Voyager\UserSiteController@deploy', 'as' => 'VoyegarUserSiteDeploy']);


   	//Site Deploy Script
   	Route::get('user-sites-deploy-script/{id}', ['uses' => 'Voyager\UserSiteController@deployScriptView', 'as' => 'VoyegarUserSiteDeployScript']);
   	Route::put('user-sites-deploy-script/{id}', ['uses' => 'Voyager\UserSiteController@deployScript', 'as' => 'VoyegarUserSiteDeployScript']);

   	//Deploy Now
   	Route::get('user-sites-deploy-now/{id}', ['uses' => 'Voyager\UserSiteController@deployNow', 'as' => 'VoyegarUserSiteDeployNow']);

	//Site Env File
   	Route::get('user-sites-env/{id}', ['uses' => 'Voyager\UserSiteController@envView', 'as' => 'VoyegarUserSiteEnvUpdate']);
   	Route::put('user-sites-env/{id}', ['uses' => 'Voyager\UserSiteController@envUpdate', 'as' => 'VoyegarUserSiteEnvUpdate']);   	

    Route::post('user-sites-create-ajax', ['uses' => 'Voyager\UserSiteController@createAjax', 'as' => 'VoyegarUserSiteCreateAjax']);
    Route::post('user-sites-install-repo-ajax', ['uses' => 'Voyager\UserSiteController@repoAjax', 'as' => 'VoyegarUserSiteInstallRepoAjax']);
    Route::post('user-sites-get_deploy-script-ajax', ['uses' => 'Voyager\UserSiteController@getDeployScriptAjax', 'as' => 'VoyegarUserSiteGetDeployScriptAjax']);
    Route::post('user-sites-deploy-script-ajax', ['uses' => 'Voyager\UserSiteController@deployScriptAjax', 'as' => 'VoyegarUserSiteDeployScriptAjax']);
    Route::post('user-sites-get-env-ajax', ['uses' => 'Voyager\UserSiteController@getEnvAjax', 'as' => 'VoyegarUserSiteGetEnvAjax']);
    Route::post('user-sites-update-env-ajax', ['uses' => 'Voyager\UserSiteController@updateEnvAjax', 'as' => 'VoyegarUserSiteUpdateEnvAjax']);
   	
});

// Include Wave Routes
Wave::routes();

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('config:clear ');
    $exitCode = Artisan::call('view:clear');
    return "Cache is cleared";
});